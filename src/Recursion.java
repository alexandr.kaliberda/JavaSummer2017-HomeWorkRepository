/*
Написать рекурсивную процедуру для вывода на экран цифр натурального числа в обратном порядке.
*/

import java.util.Scanner;
public class Recursion {
    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        int naturalNumber=0;

        System.out.print("Please enter natural number: ");
        for (int i = 0; i < 1; i++) {//to get natural value properly
            if (sc.hasNextInt()) {
                naturalNumber=sc.nextInt();
            }
            else {
                System.err.print("Sorry, entered value is not correct. " +
                        "Please enter natural number: ");
                sc.next();
                i--;
            }
        }
        int counterOfNumbers = checkAmntOfNumbers(naturalNumber);

        System.out.print("Recursion value is: ");
        System.out.println(backwardsRecursion("", naturalNumber,counterOfNumbers));

    }

    static int checkAmntOfNumbers(int naturalNumber){//Method checking amount of numbers in given value
        int counter=0;
        while(naturalNumber!=0){
            naturalNumber/=10;
            counter++;
        }
        return counter;
    }

    static int backwardsRecursion(String str, int naturalNumber, int counterOfNumbers){//Method to print recursion backwards
        if(counterOfNumbers==0){
            return Integer.parseInt(str);
        }
        counterOfNumbers--;

        return backwardsRecursion(str+=naturalNumber%10, naturalNumber/=10,counterOfNumbers);
    }

}
