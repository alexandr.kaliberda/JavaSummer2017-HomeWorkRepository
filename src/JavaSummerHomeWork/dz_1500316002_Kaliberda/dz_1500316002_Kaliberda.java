package JavaSummerHomeWork.dz_1500316002_Kaliberda;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;
public class dz_1500316002_Kaliberda {
    public static void main(String[] args) {
        //Условие задачи dz_1500316002
        /*Видео: https://youtu.be/9KnNAlc4SGg

        В одном из предыдущих домашних заданий разбить программу на методы.
        Текст задания:
        ------------------------------------------------------------------
                Используя сортировку выбором, пузырька и вставки
        отсортируйте массив введенный пользователем с клавиатуры
        (до 4 элементов).

        Вывод прогаммы должен быть следующим:

        Сортировка выбором:
        Исходный массив:  8 6 4 2
        1-й проход: 	2 6 4 8
        2-й проход: 	2 4 6 8
        3-й проход: 	2 4 6 8

        Пузырьковая сортировка:
        Исходный массив: 8 6 4 2
        1-й проход: 	6 8 4 2
        6 4 8 2
        6 4 2 8
        2-й проход: 	4 6 2 8
        4 2 6 8
        3-й проход: 	2 4 6 8

        Сортировка вставками:
        Исходный массив: 8 6 4 2
        1-й проход: 	6 8 4 2
        2-й проход: 	4 6 8 2
        3-й проход: 	2 4 6 8*/

        Scanner sc = new Scanner(System.in);
        int[] array = new int[4];

        System.out.println("Please type 4 numbers");
        array= getArray(array);

        System.out.println(Arrays.toString(array)+" Сортировка выборкой. Исходный Массив");
        selectionSort(array);

        System.out.println();

        System.out.println(Arrays.toString(array)+" Пузырьковая сортировка. Исходный Массив");
        bubbleSort(array);

        System.out.println();

        System.out.println(Arrays.toString(array)+" Сортировка вставкой. Исходный Массив");
        insertionSort(array);
    }//Method main

    static void bubbleSort(int[]array) {
        int[]arr= copyArray(array);

        for (int i = arr.length-1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
                System.out.println(Arrays.toString(arr));
            }
        }
    }//Bubble sort's method

    static void selectionSort(int[]array){
        int[]arr= copyArray(array);

        for (int i = 0; i < arr.length-1; i++) {
            int min = i;
            for (int j = i+1; j < arr.length; j++) {
                if(arr[min]>arr[j])
                    min=j;
            }
            int temp=arr[min];
            arr[min]=arr[i];
            arr[i]=temp;
            System.out.println(Arrays.toString(arr));
        }
    }//Selection sort's method

    static void insertionSort(int[]array){
        int[]arr= copyArray(array);

        for (int i = 1; i < arr.length; i++) {
            int temp=arr[i],j;
            for (j = i-1; j >= 0 && arr[j]>temp ; j--) {
                arr[j+1]=arr[j];
            }
            arr[j+1]=temp;
            System.out.println(Arrays.toString(arr));
        }
    }//Insertion sort's method

    static int[] getArray(int[]array){
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < array.length; i++) {
            try {
                array[i] = sc.nextInt();
            }catch(InputMismatchException e){
                System.out.println("Sorry, written value is not number. Please try again");
                sc.nextLine();
                i--;
            }
        }
        return array;
    }//Method that will get 4 numbers from user

    static int[] copyArray(int[]array) {
        int arr[] = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            arr[i] = array[i];
        }
        return arr;
    }//Method that prevent initial array from rewrite
}
