package JavaSummerHomeWork.GumArrayPackage;

//HW_1504549217
//https://youtu.be/PhsYtG-fTvw
//Создать класс резинового массива, который умеет расширяться или сужаться по необходимости

import java.util.Arrays;
import java.util.Scanner;

public class GumArrayMain {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        GumArray gumArray = new GumArray();
        System.out.println("Test of gum array will be performed.\nPlease write 1 to show the test, or any key to exit");

        //In this part, checking user's choice.
        //1 to look at gum array tests, otherwise any button to exit the program
        int choice;
        if(!sc.hasNextInt())
            choice=2;
        else choice = sc.nextInt();

        if(choice==1){

            //Test part start's from here
            {
                System.out.println("Test of getGumArray() getter will be performed");
                int[] testedArray = {1, 2, 3};
                boolean result = true;

                gumArray.addToArray(testedArray);
                int[] expectedResultsArray = gumArray.getGumArray();
                for (int i = 0; i < testedArray.length; i++) {
                    if (expectedResultsArray[i] != testedArray[i]) result = false;
                }
                if(!result) System.err.println("Test was not passed.");
                else System.out.println("Test passed.");
                gumArray.cleanArray();
            }

            System.out.println("*******************************************************************");

            {
                System.out.println("Test of overloaded addToArray(int[]) method will be performed");
                int[] testedArray = {1, 2, 3};
                boolean result = true;

                gumArray.addToArray(testedArray);
                int[] expectedResultsArray = gumArray.getGumArray();
                for (int i = 0; i < testedArray.length; i++) {
                    if (expectedResultsArray[i] != testedArray[i]) result = false;
                }
                if(!result) System.err.println("Test was not passed.");
                else System.out.println("Test passed.");
                gumArray.cleanArray();
            }

            System.out.println("*******************************************************************");

            {
                System.out.println("Test of overloaded addToArray(int) method will be performed");
                int[] testedValue = {1,2,3,4,5,6,7,8,9,10,11};
                boolean result = true;

                for (int i = 0; i < testedValue.length; i++) {
                    gumArray.addToArray(testedValue[i]);
                }
                int[] expectedResult = {1,2,3,4,5,6,7,8,9,10,11,0,0,0,0,0,0,0,0,0};
                for (int i = 0; i < expectedResult.length; i++) {
                    if (expectedResult[i]!=gumArray.getGumArray()[i])result = false;
                }
                if(!result) System.err.println("Test was not passed.");
                else System.out.println("Test passed.");
            }

            System.out.println("*******************************************************************");

            {
                System.out.println("Test of dropElementsFromLeftSide() method will be performed");
                int[] testedArray = {1,2,3,4};
                boolean result = true;

                int[] expectedResultsArray = {2,3,4};
                gumArray.addToArray(testedArray);
                gumArray.dropElementsFromLeftSide(1);

                for (int i = 0; i < expectedResultsArray.length; i++) {
                    if (expectedResultsArray[i] != gumArray.getGumArray()[i]) result = false;
                }
                if(!result) System.err.println("Test was not passed.");
                else System.out.println("Test passed.");
                gumArray.cleanArray();
            }

            System.out.println("*******************************************************************");

            {
                System.out.println("Test of dropElementsFromRightSide() method will be performed");
                int[] testedArray = {1,2,3,4};
                boolean result = true;

                int[] expectedResultsArray = {1,2,3};
                gumArray.addToArray(testedArray);
                gumArray.dropElementsFromRightSide(1);

                for (int i = 0; i < expectedResultsArray.length; i++) {
                    if (expectedResultsArray[i] != gumArray.getGumArray()[i]) result = false;
                }
                if(!result) System.err.println("Test was not passed.");
                else System.out.println("Test passed.");
                gumArray.cleanArray();
            }

            System.out.println("*******************************************************************");

            {
                System.out.println("Test of showElementsInArray() method will be performed.\n" +
                        "Visual comparation only.");
                int[] testedArray = {1,2,3,4};

                System.out.println(Arrays.toString(testedArray));
                gumArray.addToArray(testedArray);
                gumArray.showElementsInArray();
            }

            System.out.println("\n*******************************************************************");

            {
                System.out.println("Test of cleanArray() method will be performed.\n" +
                        "Visual comparation only. Expected no values in array.");
                int[]testedArray={1,2,3};
                gumArray.addToArray(testedArray);
                gumArray.cleanArray();
                gumArray.showElementsInArray();
            }
        }
        System.out.println("End of program");
    }
}
