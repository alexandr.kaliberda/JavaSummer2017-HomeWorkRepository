package JavaSummerHomeWork.GumArrayPackage;

import java.util.Arrays;

class GumArray {

    /**
     * Array that initially has no elements.
     * It used by methods: addToArray(int[]), addToArray(int), dropElementsFromLeftSide, dropElementsFromRightSide
     * showElementsInArray, cleanArray.
     *
     * @see
     */
    private int[] gumArray;

    /**
     * The counter is used for addToArray(int element) method
     * to add new elements to array or rewrite elements from
     * old array to new array.
     *
     * @see
     */
    private int counterForAddToArrayMethod;

    public int[] getGumArray() {
        return gumArray.clone();
    }

    /**
     * Gets counterForAddToArrayMethod's value.
     * Used for addToArray[int element] overloaded method
     *
     * @see
     */
    public int getCounter() {
        return counterForAddToArrayMethod;
    }

    /**
     * Sets counterForAddToArrayMethod's value.
     * Used for addToArray[int element] overloaded method
     *
     * @see
     */
    public void setCounter(int counter) {
        this.counterForAddToArrayMethod = counter;
    }


    /**
     * As parameter receives whole array.
     * As result, adds received array to existing array.
     * Example: Existed: 1,2,3. Added: 4,5,6. Result: 4,5,6,1,2,3
     *
     * @see
     */
    public void addToArray(int[] array) {
        int[]temp=new int[0];

        if(gumArray!=null)temp = gumArray;

        gumArray = new int[temp.length + array.length];

        for (int i = 0; i < array.length; i++) {
            gumArray[i] = array[i];
        }

        int counter = 0;
        for (int i = array.length; i < gumArray.length; i++) {
            gumArray[i] = temp[counter++];
        }
    }


    /**
     * Used to add elements to array.
     * Array can dynamically increase it's size depend on array load.
     * As parameter receives single int value.
     * If after adding value, array will be overloaded,
     * array size [existed elements + 10] will be created.
     * Example: Existed in array: 1,2,3,4,5,6,7,8,9,10. Added: 11.
     * Result: 1,2,3,4,5,6,7,8,9,10,11,0,0,0,0,0,0,0,0,0
     *
     * @see
     */
    public void addToArray(int element) {
        int counter = getCounter();
        int[] temp;

        //For case if addToArray() method used for first time,
        //create 10 elements array and assign value to 1st element
        if (gumArray==null || gumArray.length==0) {
            gumArray = new int[10];
            gumArray[0] = element;
            setCounter(++counter);
        }

        //For case if addToArray() method is used not for first time
        else {
            //if array is almost overloaded, create new
            //[exist. elements + 10 elements] array
            if (counter == gumArray.length) {
                temp = getGumArray();
//                temp = gumArray.clone();
                gumArray = new int[temp.length + 10];

                //overwrite from old array to new array
                for (int i = 0; i < counter; ++i) {
                    gumArray[i] = temp[i];
                }
                //assign new value to last element of array
                gumArray[counter] = element;
            }

            //if array is not overloaded, add next element to array
            else {
                gumArray[counter++] = element;
                setCounter(counter);
            }
        }
    }


    /**
     * As parameter receives value that will be used-
     * to drop specified amount of elements from left side of array.
     * Example: Existed: 1,2,3,4. Specified to remove: 1. Result: 2,3,4.
     * Array size will be decreased as well to existing elements - 1.
     *
     * @see
     */
    public void dropElementsFromLeftSide(int amountElementsToDrop) {
        if (amountElementsToDrop < gumArray.length) {
            int[] temp = gumArray;

            gumArray = new int[temp.length - amountElementsToDrop];

            for (int i = 0; i < gumArray.length; i++) {
                gumArray[i] = temp[i + amountElementsToDrop];
            }
        } else gumArray = new int[0];
    }


    /**
     * As parameter receives value that will be used-
     * to drop specified amount of elements from right side of array.
     * Example: Existed: 1,2,3,4. Specified to remove: 1. Result: 1,2,3
     * Array size will be decreased as well to existing elements - 1.
     *
     * @see
     */
    public void dropElementsFromRightSide(int amountElementsToDrop) {

        if (amountElementsToDrop < gumArray.length) {
            int[] temp = gumArray;

            gumArray = new int[temp.length - amountElementsToDrop];

            for (int i = 0; i < gumArray.length; i++) {
                gumArray[i] = temp[i];
            }
        } else gumArray = new int[0];
    }


    /**
     * Prints/shows to console all existing elements in array
     * Example: In array it has elements 1,2,3,4. It will print 1,2,3,4 to console
     *
     * @see
     */
    public void showElementsInArray() {
        System.out.println(Arrays.toString(getGumArray()));
    }

    /**
     * Cleans existing values of all elements is array, and makes array size as '0'
     * Example: In array it has elements 1,2,3,4. After calling the mothod, as result, 0 elemets in array will be.
     *
     * @see
     */
    public void cleanArray() {
        gumArray = new int[0];
    }
}
