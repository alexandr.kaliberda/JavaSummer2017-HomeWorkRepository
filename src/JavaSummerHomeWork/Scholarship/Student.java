package JavaSummerHomeWork.Scholarship;

public class Student {
    private String familyName;
    private String name;
    private String middleName;
    private int groupNumber;
    private int[]examinationMarks=new int[5];
    private int premium;
    private boolean scholarshipBoolean;

    Student() {
        this("Unknown family name", "Unknown name","Unknown middle name",
                0,0,
                0,0,0,0);
    }

    Student(String _familyName) {
        this(_familyName, "name","Unknown middle name",0,0,
                0,0,0,0);
    }

    Student(String _familyName, String _name) {
        this(_familyName,_name,"Unknown middle name",0,0,
                0,0,0,0);
    }

    Student(String _familyName, String _name, String _middleName) {
        this(_familyName,_name,_middleName,0,0,
                0,0,0,0);
    }

    Student(String familyName, String name, String middleName, int groupNumber, int examinationMarks_1,
            int examinationMarks_2, int examinationMarks_3, int examinationMarks_4, int examinationMarks_5) {
        this.familyName = familyName;
        this.name = name;
        this.middleName = middleName;
        this.groupNumber = groupNumber;
        this.examinationMarks[0] = examinationMarks_1;
        this.examinationMarks[1] = examinationMarks_2;
        this.examinationMarks[2] = examinationMarks_3;
        this.examinationMarks[3] = examinationMarks_4;
        this.examinationMarks[4] = examinationMarks_5;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    public int getExaminationMarks_1() {
        return examinationMarks[0];
    }

    public void setExaminationMarks_1(int examinationMarks_1) {
        this.examinationMarks[0] = examinationMarks_1;
    }

    public int getExaminationMarks_2() {
        return examinationMarks[1];
    }

    public void setExaminationMarks_2(int examinationMarks_2) {
        this.examinationMarks[1] = examinationMarks_2;
    }

    public int getExaminationMarks_3() {
        return examinationMarks[2];
    }

    public void setExaminationMarks_3(int examinationMarks_3) {
        this.examinationMarks[2] = examinationMarks_3;
    }

    public int getExaminationMarks_4() {
        return examinationMarks[3];
    }

    public void setExaminationMarks_4(int examinationMarks_4) {
        this.examinationMarks[3] = examinationMarks_4;
    }

    public int getExaminationMarks_5() {
        return examinationMarks[4];
    }

    public void setExaminationMarks_5(int examinationMarks_5) {
        this.examinationMarks[4] = examinationMarks_5;
    }

    public int getPremium() {
        return premium;
    }

    public void setPremium(int premium) {
        this.premium = premium;
    }

    public boolean getScholarshipBoolean() {
        return scholarshipBoolean;
    }

    public void setScholarshipBoolean(boolean scholarshipBoolean) {
        this.scholarshipBoolean = scholarshipBoolean;
    }

    public void assignPremium(Student student) {

        final int PREMIUM_100_PERCENTS = 100;
        final int PREMIUM_50_PERCENTS = 50;
        final int PREMIUM_25_PERCENTS = 25;
        final int PREMIUM_0_PERCENTS = 0;

        int counterSatisfactorilyMark = 0, counterGoodMark = 0, counterPerfectMark = 0;

        int[] temp = new int[examinationMarks.length];//Array that temporarily stores given students examination marks

        for (int i = 0; i < examinationMarks.length; i++) {
            temp[i]=student.examinationMarks[i];
        }

        for (int i = 0; i < temp.length; i++) {
            switch (temp[i]) {
                case 3:
                    counterSatisfactorilyMark++;
                    break;
                case 4:
                    counterGoodMark++;
                    break;
                case 5:
                    counterPerfectMark++;
                    break;
                default:
                    System.err.println("Exam marks input error. Please start from beginning");
            }
        }

        if (counterPerfectMark == 5) {
            student.premium = PREMIUM_100_PERCENTS;
            student.scholarshipBoolean = true;
        }
        if (counterSatisfactorilyMark == 0 && counterGoodMark > 0 && counterPerfectMark > 0) {
            student.premium = PREMIUM_50_PERCENTS;
            student.scholarshipBoolean = true;
        }
        if (counterGoodMark == 5) {
            student.premium = PREMIUM_25_PERCENTS;
            student.scholarshipBoolean = true;
        }
        if (counterSatisfactorilyMark > 0 && counterSatisfactorilyMark < 3) {
            student.premium = PREMIUM_0_PERCENTS;
            student.scholarshipBoolean = true;
        }

        if (counterSatisfactorilyMark > 3) {
            student.premium = PREMIUM_0_PERCENTS;
            student.scholarshipBoolean = false;
        }
}

    public void printStudents(Student[] student) {
        System.out.printf("#\tFamily Name\t\t\tName\t\t\t\tMiddle Name \t\tGroupNum \tExam1\tExam2 \tExam3 \tExam4 \tExam5 \tStudent \tPremium%n");

        for (int i = 0; i < student.length; i++) {
            if (student[i].getScholarshipBoolean()) {
                System.out.printf("%d\t %-15s, \t%-15s, \t%-15s, \t%d, \t\t\t%d, \t\t%d, \t\t%d, \t\t%d, \t\t%d, \t\t%b, \t\t\t%d%n",
                        i + 1, student[i].getFamilyName(), student[i].getName(), student[i].getMiddleName(), student[i].getGroupNumber(),
                        student[i].getExaminationMarks_1(), student[i].getExaminationMarks_2(), student[i].getExaminationMarks_3(),
                        student[i].getExaminationMarks_4(), student[i].getExaminationMarks_5(), student[i].getScholarshipBoolean(), student[i].getPremium());
            }
        }
    }

    public boolean checkMakrs(Student[] student) {
        for (int i = 0; i < student.length; i++) {

            int[] temp = new int[5];//Array that temporarily stores given students examination marks

            temp[0] = student[i].examinationMarks[0];
            temp[1] = student[i].examinationMarks[1];
            temp[2] = student[i].examinationMarks[2];
            temp[3] = student[i].examinationMarks[3];
            temp[4] = student[i].examinationMarks[4];

            for (int j = 0; j < temp.length; j++) {
                if (temp[j] < 3 || temp[j] > 5) return false;
            }
            if (student[i].groupNumber <= 0) return false;
        }
        return true;
    }

}
