package JavaSummerHomeWork.Scholarship;
//HW_1504286853 from 01.09.2017
//Составить программу для начисления стипендии студентам по результатам экзаменационной сессии.
//Информация о результатах сессии включает в себя:
//– фамилию;
//– имя;
//– отчество;
//– номер группы;
//– экзаменационные оценки.
//Количество экзаменационных оценок не менее 3 и не более 5. Стипендия начисляется студентам,
//сдавшим все экзамены в сессию, по следующим правилам. Студенты, сдавшие все экзамены на "отлично"
//получают надбавку равную 100%; студенты, сдавшие экзамены на "хорошо" и "отлично" – 50%; а студенты,
//сдавшие экзамены на "хорошо", – 25%. Стипендия не начисляется студентам, имеющим в сессию более двух
//удовлетворительных оценок.
//Список студентов каждой группы, получивших стипендию, вывести на экран, упорядочив его по алфавиту.


import java.util.Arrays;
import java.util.Scanner;

public class MainScholarship {
    //Sort parameters
    static final int FAMILY_NAME=1, NAME=2, MIDDLE_NAME =3, GROUP_NUMBER=4, EXAMINATION_MARKS_1=5, EXAMINATION_MARKS_2=6,
            EXAMINATION_MARKS_3=7, EXAMINATION_MARKS_4=8, EXAMINATION_MARKS_5=9;

    public static void main(String[] args) {

        String[] familyName = {"MILLER", "SHEVCHUK", "IVANOVA", "KRAVCHUK", "LISENKO", "SAVCHENKO", "KOVALENKO", "FROST", "PONOMARENKO", "Rudenko", "KHOMENKO", "Kharchenko", "TKACHENKO", "MELINICHUK", "KOVAL", "COLOMY", "LITENKO", "KLIMENKO", "SHEVCHENKO", "Kravchenko", "MARCHENKO", "PETRENKO", "KOVALCHUK", "GAVRILYUK", "LEVCHENKO", "PAVLYUK", "ROMANYUK", "BONDARENKO", "Pavlenko", "SAVCHUK", "OLIVNIK", "KARPENKO", "BOYKO", "BONDAR", "VASILENKO", "MARTINYUK", "WEAVER", "MAZUR", "KUZMENKO", "TKACHUK", "KUSHNIR", "POLISHCHUK", "SIDORENKO"};
        String[] name = {"Dmitry", "Solomiya", "Maria", "Veronika", "Andrew", "Anastasia", "Michael", "Daniel", "Ivan", "Dmitry", "Daniel", "Solomiya", "Vladislav", "Eva", "Ivan", "Solomiya", "Artem", "Victoria", "Veronika", "Daria", "Zlata", "Dmitry", "Artem", "Daria", "Zlata", "Ivan", "Daria", "Denis", "Michael", "Denis", "Maria", "Andrew", "Victoria", "Alexander", "Alexander", "Maria", "Eva", "Sofia", "Vladislav", "Sofia", "Dmitry", "Eva", "Anastasia"};
        String[] middleName = {"Andrew", "Artem", "Eva", "Anastasia", "Dmitry", "Sofia", "Alexander", "Daria", "Daniel", "Veronika", "Daria", "Artem", "Zlata", "Maria", "Daniel", "Denis", "Ivan", "Victoria", "Anastasia", "Solomiya", "Vladislav", "Andrew", "Ivan", "Solomiya", "Vladislav", "Daria", "Maria", "Michael", "Alexander", "Michael", "Eva", "Dmitry", "Victoria", "Dmitry", "Dmitry", "Dmitry", "Vladislav", "Denis", "Zlata", "Denis", "Veronika", "Maria", "Sofia"};
        int[] groupNumber = {2, 1, 1, 4, 3, 2, 3, 3, 3, 4, 4, 4, 2, 1, 3, 4, 3, 2, 4, 4, 4, 2, 2, 3, 1, 1, 4, 3, 4, 1, 1, 1, 2, 4, 1, 4, 1, 1, 1, 4, 4, 4, 4};
        int[] examinationMarks_1 = {5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 4, 5, 5, 5, 5, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 4, 5, 5, 4, 5, 5, 5, 5, 5, 5, 4, 5, 5, 5, 5};
        int[] examinationMarks_2 = {5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 4, 5, 5, 5, 5, 4, 5, 5, 5, 5, 5, 5, 5, 3, 5, 5, 5, 5, 4, 5, 5, 4, 5, 5, 5, 5, 5, 5, 4, 5, 5, 5, 5};
        int[] examinationMarks_3 = {5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 4, 5, 5, 3, 5, 4, 5, 5, 5, 5, 5, 5, 5, 3, 5, 5, 5, 5, 4, 5, 3, 4, 5, 5, 5, 3, 5, 5, 4, 5, 5, 3, 5};
        int[] examinationMarks_4 = {4, 4, 3, 4, 5, 5, 4, 5, 5, 3, 4, 4, 4, 3, 5, 4, 3, 3, 4, 4, 3, 3, 3, 4, 5, 3, 5, 4, 4, 3, 3, 4, 5, 4, 5, 3, 5, 3, 4, 3, 3, 4, 4};
        int[] examinationMarks_5 = {5, 5, 5, 5, 4, 3, 5, 5, 3, 3, 4, 5, 3, 3, 5, 4, 5, 4, 3, 5, 4, 4, 5, 3, 5, 3, 5, 3, 4, 4, 3, 4, 4, 5, 5, 3, 3, 3, 4, 3, 3, 5, 3};

        int groupSize = familyName.length;

        Student[] student = new Student[groupSize];

        //Initializing array of students data by arrays (familyName, name, middleName, groupNumber and exam marrks)
        for (int i = 0; i < groupSize; i++) {
            student[i] = new Student(familyName[i], name[i], middleName[i], groupNumber[i], examinationMarks_1[i],
                    examinationMarks_2[i], examinationMarks_3[i], examinationMarks_4[i], examinationMarks_5[i]);
        }

        Student temp = new Student();

        //Procedd if students exam marks more than 2 and less than 6, and if group is more than 0
        if(temp.checkMakrs(student)) {

            System.out.printf("Group size is: %d student/s%n",groupSize);


            sortStudents(student, FAMILY_NAME);//Sort by Family name

            sortStudents(student, GROUP_NUMBER);//Sort by group name after sorting by Family name, gives students sorted
            //by family name and group number

            for (int i = 0; i < groupSize; i++) {
                temp.assignPremium(student[i]);
            }

            System.out.println();

            System.out.println("List of students to receive scholarship:");
            temp.printStudents(student);

        }else System.err.println("Please cross check student marks or stuend group.%n" +
                "Marks should not be less than 3 and greater than 5.%n" +
                "Group should be more than 0");


        System.out.println("Press 1 to quit or 2 to watch tests of methods");
        Scanner sc = new Scanner(System.in);
        int choice = sc.nextInt();

        if(choice==2) {

            {
                System.out.println("assignPremium method's test. First student 100% premium, Second student 50% premium, Third student 25% premium" +
                        ". Forth student 0% premium");

                String[] familyName1 = {"MILLER", "SHEVCHUK", "IVANOVA", "KRAVCHUK"};
                String[] name1 = {"Dmitry", "Solomiya", "Maria", "Veronika"};
                String[] middleName1 = {"Andrew", "Artem", "Eva", "Anastasia"};
                int[] groupNumber1 = {2, 1, 1, 4};
                int[] examinationMarks1 = {5, 4, 4, 3};
                int[] examinationMarks2 = {5, 4, 4, 3};
                int[] examinationMarks3 = {5, 5, 4, 4};
                int[] examinationMarks4 = {5, 5, 4, 5};
                int[] examinationMarks5 = {5, 5, 4, 5};

                int[] expectedPremium = {100, 50, 25, 0};

                Student temp1 = new Student();
                Student[] studentTest = new Student[groupNumber1.length];

                //Initializing array of students data by arrays (familyName, name, middleName, groupNumber and exam marrks)
                for (int i = 0; i < familyName1.length; i++) {
                    studentTest[i] = new Student(familyName1[i], name[i], middleName1[i], groupNumber1[i], examinationMarks1[i],
                            examinationMarks2[i], examinationMarks3[i], examinationMarks4[i], examinationMarks5[i]);
                }

                for (int i = 0; i < familyName1.length; i++) {
                    temp1.assignPremium(studentTest[i]);
                }

                temp1.printStudents(studentTest);

                for (int i = 0; i < familyName1.length; i++) {
                    if(studentTest[i].getPremium()==expectedPremium[i])
                        System.out.printf("%d Student premium = %d equals to expectedPremium = %d%n", i+1, studentTest[i].getPremium(),expectedPremium[i]);
                    else System.out.println("Test was not passed");
                }
            }

            {
                System.out.println("\nassignPremium method's test. Test of Student that will not get Student. " +
                        "The student should not come to list as well.");

                String[] familyName1 = {"MILLER"};
                String[] name1 = {"Dmitry"};
                String[] middleName1 = {"Andrew"};
                int[] groupNumber1 = {2};
                int[] examinationMarks1 = {3};
                int[] examinationMarks2 = {3};
                int[] examinationMarks3 = {3};
                int[] examinationMarks4 = {4};
                int[] examinationMarks5 = {5};

                boolean expectedStipendiya = false;

                Student temp1 = new Student();
                Student[] studentTest = new Student[groupNumber1.length];

                //Initializing array of students data by arrays (familyName, name, middleName, groupNumber and exam marrks)
                for (int i = 0; i < familyName1.length; i++) {
                    studentTest[i] = new Student(familyName1[i], name[i], middleName1[i], groupNumber1[i], examinationMarks1[i],
                            examinationMarks2[i], examinationMarks3[i], examinationMarks4[i], examinationMarks5[i]);
                }

                for (int i = 0; i < familyName1.length; i++) {
                    temp1.assignPremium(studentTest[i]);
                }

                temp1.printStudents(studentTest);

                for (int i = 0; i < familyName1.length; i++) {
                    if(studentTest[i].getScholarshipBoolean()==expectedStipendiya)
                        System.out.printf("%d Student stpendiya = %b equals to expectedStipendiya = %b%n",
                                i+1, studentTest[i].getScholarshipBoolean(),expectedStipendiya);
                    else System.out.println("Test was not passed");
                }
            }

            {
                System.out.println("\ncheckMakrs method's test. Checking wrong examination mark" +
                        ". Student has -1 in examination marks");

                String[] familyName1 = {"MILLER"};
                String[] name1 = {"Dmitry"};
                String[] middleName1 = {"Andrew"};
                int[] groupNumber1 = {2};
                int[] examinationMarks1 = {-1};
                int[] examinationMarks2 = { 5};
                int[] examinationMarks3 = { 5};
                int[] examinationMarks4 = { 5};
                int[] examinationMarks5 = { 5};

                boolean[] expectedWrongResult = {false};

                Student temp1 = new Student();
                Student[] studentTest = new Student[groupNumber1.length];

                //Initializing array of students data by arrays (familyName, name, middleName, groupNumber and exam marrks)
                for (int i = 0; i < familyName1.length; i++) {
                    studentTest[i] = new Student(familyName1[i], name[i], middleName1[i], groupNumber1[i], examinationMarks1[i],
                            examinationMarks2[i], examinationMarks3[i], examinationMarks4[i], examinationMarks5[i]);
                }
                System.out.println("Student wrong mark result = "+temp1.checkMakrs(studentTest)+". Expected wrong mark result = "+Arrays.toString(expectedWrongResult));
            }
        }
        else System.out.println("End of program.");
    }


    public static void sortStudents(Student[] student, int sortParameter) {
        int groupAmount = student.length;
        for (int i = 0; i < groupAmount; i++) {
            for (int j = 1; j < (groupAmount - i); j++) {
                if (compareStudents(student[j - 1], student[j], sortParameter)) {
                    Student temp = student[j - 1];
                    student[j - 1] = student[j];
                    student[j] = temp;
                }
            }
        }
    }

    public static boolean compareStudents(Student student1, Student student2, int sortParameter) {

        switch (sortParameter) {
            case 1://CompareBy student familyName
                return student1.getFamilyName().compareTo(student2.getFamilyName()) > 0;
            case 2://CompareBy student name
                return student1.getName().compareTo(student2.getName()) > 0;
            case 3://CompareBy student middleName
                return student1.getMiddleName().compareTo(student2.getMiddleName()) > 0;
            case 4://CompareBy student groupNumber
                return student1.getGroupNumber() > student2.getGroupNumber();
            case 5://CompareBy student examinationMarks_1
                return student1.getExaminationMarks_1() > student2.getExaminationMarks_1();
            case 6://CompareBy student examinationMarks_2
                return student1.getExaminationMarks_2() > student2.getExaminationMarks_2();
            case 7://CompareBy student examinationMarks_3
                return student1.getExaminationMarks_3() > student2.getExaminationMarks_3();
            case 8://CompareBy student examinationMarks_4
                return student1.getExaminationMarks_4() > student2.getExaminationMarks_4();
            case 9://CompareBy student examinationMarks_5
                return student1.getExaminationMarks_5() > student2.getExaminationMarks_5();
        }
        return false;
    }
}
