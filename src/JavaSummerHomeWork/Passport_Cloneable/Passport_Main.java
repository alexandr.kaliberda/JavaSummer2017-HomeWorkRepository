package JavaSummerHomeWork.Passport_Cloneable;

//Passport_Cloneable_HW_1504722319
//Добавить возможность глубокого клонирования загранпаспорта, описанного в одном из предыдущих ДЗ.

class Passport_Main {

    public static void main(String[] args) {

        System.out.println("TEST PART *** TEST PART *** TEST PART *** TEST PART *** TEST PART *** TEST PART *** \n");

        //Remained from previous home work
//        {
//            System.out.println("Test of stickNewVisaToFPassport() method.");
//            System.out.println("Test_1 Expected: Adding first and second visas to passport, without errors.\n" +
//                    "Passport has space for 2 visas only. Visual comparation only.");
//
//            //Initializing foreign passport by name, family name, birth day.
//            FPassport f1 = new FPassport("Andrey","Kuzmenko",10,1,1995,"MU123");
//            FPassport f2 = new FPassport("Artem","Tkachuk",11,2,1996,"MU124");
//            FPassport f3 = new FPassport("Eva","Polishuk",12,3,1997,"MU125");
//
//            //Initializing foreign passport by first viza
//            f1.stickNewVisaToFPassport("Business",11,10,2019,11,11,2019);
//            f2.stickNewVisaToFPassport("Business",11,10,2019,11,10,2020);
//            f3.stickNewVisaToFPassport("Student",11,10,2019,21,10,2019);
//
//            //Initializing foreign passport by second viza
//            f1.stickNewVisaToFPassport("Business",11,11,2019,11,11,2021);
//            f2.stickNewVisaToFPassport("Business",11,10,2020,11,10,2021);
//            f3.stickNewVisaToFPassport("Student",21,10,2019,21,10,2020);
//
//            //Creating array of foreign passports
//            FPassport[] fp = {f1,f2,f3};
//
//            System.out.println("*********************************************************************************");
//            System.out.println("Page Visa-holder_______   FPSerial  VisaIssued  ValidUntill VisaType");
//            for (int i = 0; i < fp.length; i++) {
//                if (fp[i] != null)
//                    lookAtVisaPages(fp[i]);
//            }
//
//            System.out.println("Test passed.\n");
//        }
//
//        {
//            System.out.println("*********************************************************************************");
//            System.out.println("Test_2. Adding third visa inside of two pages passport\n" +
//                    "Expected: Warning message and request to create new visas.\n" +
//                    "Visual comparison only.");
//
//            FPassport fp1 = new FPassport("Andrew","KUZMENKO",10,1,1995,"MU123");
//            fp1.stickNewVisaToFPassport("Student_1",16,10,2017,16,10,2018);
//            fp1.stickNewVisaToFPassport("Student_2",16,10,2018,16,10,2019);
//            fp1.stickNewVisaToFPassport("Student_3",16,10,2019,16,10,2020);
//
//            System.out.println("Page Visa-holder_______   FPSerial  VisaIssued  ValidUntill VisaType");
//            lookAtVisaPages(fp1);
//            System.out.println("Test passed.\n");
//        }
//
//
//        {
//            System.out.println("*********************************************************************************");
//            System.out.println("Test_3. Showing first page of foreign passport\n" +
//                    "Expected: It will show Name and Family Name, Passport serial number and BirtDate.\n" +
//                    "Visual comparison only.");
//
//            FPassport fp2 = new FPassport("Petya","Vovachkin",10,1,1995,"MU123");
//            showFPsprtFirstPage(fp2);
//            System.out.println("Test passed.\n");
//        }

        {
            System.out.println("*********************************************************************************");
            System.out.println("Test_4. Cloning Foreign passport by overriden method clone()\n");

            FPassport f = new FPassport("Andrew","KUZMENKO", 10,01,1995,"MU123");
            Visa visa1 = new Visa("Student",11,10,2017,11,10,2018);
            f.stickNewVisaToFPassport(visa1);

            FPassport f1 = new FPassport();

            try{
                f1=f.clone();
            }catch (CloneNotSupportedException e){
                e.printStackTrace();
            }

            Visa visa2 = new Visa("Visitor",14,10,2017,24,10,2017);
            f1.stickNewVisaToFPassport(visa2);

            System.out.println("Cloning object-reference f.\n" +
                    "Expected: First visa page only has been added. Second visa page showing as empty.");
            System.out.println("Page Visa-holder_______   FPSerial  VisaIssued  ValidUntill VisaType");
            lookAtVisaPages(f);

            //////////////////////////////////////////////////////////////////////////////////////////
            System.out.println("After f was cloned to f1 (Foreign passport), second page added to f1.\n" +
                    "f has only 1 visa page, but f1 has 2 visa pages in foreign passport.");
            System.out.println("Page Visa-holder_______   FPSerial  VisaIssued  ValidUntill VisaType");
            lookAtVisaPages(f1);

            System.out.println();

            //////////////////////////////////////////////////////////////////////////////////////////
            System.out.println("Test of overloaded visa pages in passport. (Added visa more than visa pages)");
            Visa visa3 = new Visa("TestStudent",11,10,2017,11,10,2018);
            f1.stickNewVisaToFPassport(visa3);
        }
    }//End of method main()

    public static void lookAtVisaPages(FPassport fp){
        for (int i = 0; i < fp.getVisas().length; i++) {
            if(fp.getVisas()[i].getVisaType()!=null) {
                System.out.printf("%-5d%-10s%-12s%-10s%-12s%s\t%-30s%n",
                        i+1,
                        fp.getFirstName(),fp.getFamilyName(),
                        fp.getPassportSerial(),
                        fp.getVisas()[i].getIssuedDate(),
                        fp.getVisas()[i].getValidTillDate(),
                        fp.getVisas()[i].getVisaType());
            }
            else {
                System.out.printf("%s %s %s. Visa page %d is empty or not properly filled%n",
                        fp.getFirstName(), fp.getFamilyName(), fp.getPassportSerial(), i + 1);
                System.out.println("*********************************************************************************");
            }
        }
    }

    public static void showFPsprtFirstPage(FPassport fp){
        System.out.println("***********************************************************************");
        System.out.printf(
                        "Foreign Passport first page.%n" +
                        "Foreign Passport Serial Number: %s%n" +
                        "First Name: %s, Family Name: %s%n" +
                        "BirthDate %s\n"
                ,fp.getPassportSerial(),fp.getFirstName(),fp.getFamilyName(),fp.getBirthDate());
    }

}
