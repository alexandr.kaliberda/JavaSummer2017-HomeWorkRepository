package JavaSummerHomeWork.Passport_Cloneable;
import java.util.Calendar;
import java.util.GregorianCalendar;

class PassportUA {
    private String firstName, familyName, passportSerial;
    private GregorianCalendar birthDate;

    PassportUA(String firstName, String familyName, int bDay, int bMonth, int bYear, String passportSerial){
        this.firstName=firstName;
        this.familyName=familyName;
        this.passportSerial=passportSerial;

        birthDate=new GregorianCalendar();
        this.birthDate.set(Calendar.DAY_OF_MONTH,bDay);
        this.birthDate.set(Calendar.MONTH,bMonth);
        this.birthDate.set(Calendar.YEAR,bYear);
    }

    PassportUA(){};

    public String getFirstName() {
        return firstName;
    }
    public String getFamilyName() {
        return familyName;
    }
    public String getBirthDate(){
        return birthDate.get(Calendar.DAY_OF_MONTH)+"."+
                birthDate.get(Calendar.MONTH)+"."+
                birthDate.get(Calendar.YEAR);
    }
    public String getPassportSerial() {
        return passportSerial;
    }
}
