package JavaSummerHomeWork.Passport_Cloneable;

class FPassport extends PassportUA implements Cloneable{

    //describes amount of visa in foreign passport
    public static final int AMNT_VISA_PAGES =2;

    //used while adding new visa to foreign passport
    private int visaCounter;

    //Passport serial for foreign passport
    String passportSerial;

    FPassport(String firstName, String familyName, int bDay, int bMonth, int bYear, String passportSerial){
        super(firstName,familyName,bDay,bMonth,bYear,null);
        this.passportSerial=passportSerial;

        for (int i = 0; i < visas.length; i++) {
            visas[i]=new Visa();
        }
    }

    FPassport(){
        for (int i = 0; i < visas.length; i++) {
            visas[i]=new Visa();
        }
    }

    private Visa[] visas =new Visa[AMNT_VISA_PAGES];

    /**
     * Method clone that provides possibility to clone FPassport object as well as
     * Visa[]object array. Method clone()has been overrided for this
     * @return
     * @throws CloneNotSupportedException
     */
    public FPassport clone()throws CloneNotSupportedException{
        //Shallow cloning
        FPassport f = (FPassport)super.clone();
        f.visas = new Visa[this.visas.length];

        for (int i = 0; visas[i].issuedDate!=null||visas[i].validDate!=null; i++) {
            //Deep cloning
            f.visas[i]=this.visas[i].clone();
        }

        return f;
    }

    /**
     *Add new visa to foreign passport.
     * Shows warning if try to add visa more than pages in foreign passport
     *
     * @param visa
     */
    public void stickNewVisaToFPassport(Visa visa){

        if(visaCounter<AMNT_VISA_PAGES) {
            visas[visaCounter++] = visa;
        }else System.err.printf("***********************************************************************%n" +
                        "Cannot add visa to %s %s %s%n" +
                        "All visa pages are occupied. Please change your passport first%n" +
                        "Cancelled request is:"+
                        "Visa type: %s%n" +
                        "Requested visa from date: %s%n" +
                        "Requested visa until date: %s%n",
                getFirstName(),getFamilyName(),getPassportSerial(),
                visa.getVisaType(),visa.getIssuedDate(),visa.getValidTillDate());
    }

    public String getPassportSerial(){
        return passportSerial;
    }

    public Visa[] getVisas() {
        return visas.clone();
    }
}
