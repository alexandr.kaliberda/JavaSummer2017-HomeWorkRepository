package JavaSummerHomeWork.Passport_Cloneable;

import java.util.Calendar;
import java.util.GregorianCalendar;

class Visa implements Cloneable{
    private String visaType;
    int testVal;
    GregorianCalendar issuedDate, validDate;

    public Visa(String visaType, int issdDay, int issdMonth, int issdYear, int vldDay, int vldMonth, int vldYear) {
        this.visaType = visaType;

        issuedDate=new GregorianCalendar();
        this.issuedDate.set(Calendar.DAY_OF_MONTH,issdDay);
        this.issuedDate.set(Calendar.MONTH,issdMonth);
        this.issuedDate.set(Calendar.YEAR,issdYear);

        validDate=new GregorianCalendar();
        this.validDate.set(Calendar.DAY_OF_MONTH,vldDay);
        this.validDate.set(Calendar.MONTH,vldMonth);
        this.validDate.set(Calendar.YEAR,vldYear);
    }

    Visa(){};

    public String getVisaType() {
        return visaType;
    }

    public String getIssuedDate() {
        return issuedDate.get(Calendar.DAY_OF_MONTH)+"."+
                issuedDate.get(Calendar.MONTH)+"."+
                issuedDate.get(Calendar.YEAR);
    }

    public String getValidTillDate() {
        return validDate.get(Calendar.DAY_OF_MONTH)+"."+
                validDate.get(Calendar.MONTH)+"."+
                validDate.get(Calendar.YEAR);
    }

    /**
     * Overriden method clone() for Visa object-references
     * @return
     * @throws CloneNotSupportedException
     */
    public Visa clone()throws CloneNotSupportedException{
        Visa visa = (Visa)super.clone();
        visa.issuedDate=(GregorianCalendar)issuedDate.clone();
        visa.validDate=(GregorianCalendar)validDate.clone();
        return visa;
    }
}
