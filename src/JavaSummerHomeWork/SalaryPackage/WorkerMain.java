package JavaSummerHomeWork.SalaryPackage;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Scanner;

        /*HW_1505758763
        Построить три класса (базовый и 2 потомка), описывающих некоторых работников с почасовой оплатой
        (один из потомков) и фиксированной оплатой (второй потомок). Описать в базовом классе абстрактный
        метод для расчета среднемесячной заработной платы. Для «повременщиков» формула для расчета
        такова: «среднемесячная заработная плата = 20.8 * 8 * почасовую ставку», для работников с фиксированной
        оплатой «среднемесячная заработная плата = фиксированной месячной оплате».

        a) Упорядочить всю последовательность работников по убыванию среднемесячного заработка.
        При совпадении зарплаты – упорядочивать данные по алфавиту по имени. Вывести идентификатор
        работника, имя и среднемесячный заработок для всех элементов списка.

        b) Вывести первые 5 имен работников из полученного в пункте а) списка.

        c) Вывести последние 3 идентификатора работников из полученного в пункте а) списка.*/


public class WorkerMain {

    public static void main(String[] args) {

        int[] workerID = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        String[] workerName = {"Andrew", "Artem", "Eva", "Anastasia", "Dmitry", "Sofia", "Alexander", "Daria", "Daniel", "Veronika"};
        int[] rate = {60, 3866, 60, 3487, 53, 3394, 60, 3955, 41, 3539};

        Worker[] worker = new Worker[workerName.length];


            for (int i = 0; i < workerName.length; i++) {
                if (i % 2 == 0) {
                    worker[i] = new HourlyPaidWorker(workerID[i], workerName[i], new BigDecimal(rate[i], MathContext.DECIMAL64));
                } else
                    worker[i] = new FixedPaidWorker(workerID[i], workerName[i], new BigDecimal(rate[i], MathContext.DECIMAL64));
            }

        compareByName(worker);

        compareBySalary(worker);

        System.out.println("Sorted workers by salary and alphabyte********************************");

        printWorkersByOrder_5_UpSide_3_DownSide(worker);

        System.out.println("****************************************************************");

         System.out.println("Test of the methods will be performed.\nPlease write 1 to show the test, or any key to exit");
        Scanner sc = new Scanner(System.in);
        //In this part, checking user's choice.
        //1 to look at gum array tests, otherwise any button to exit the program
        int choice;
        if(!sc.hasNextInt())
            choice=2;
        else choice = sc.nextInt();

        if(choice==1) {

            //Test part start's from here
            {
                System.out.println("**********************************************************************");
                System.out.println("Test of printWorker(Worker[]) method will be performed.\n" +
                        "11 elements in array.");
                int[] workerID_test = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 777};
                String[] workersName_test = {"Andrew", "Artem", "Eva", "Anastasia", "Dmitry", "Sofia", "Alexander", "Daria", "Daniel", "Veronika", "Chuck"};
                int[] rate_test = {60, 3866, 60, 3487, 53, 3394, 60, 3955, 41, 3539, 9999};

                System.out.println("Expected that 5 workers at up side and 3 workers at down side will be shown.");
                Worker[] testWorkers = new Worker[workersName_test.length];

                for (int i = 0; i < testWorkers.length; i++) {
                    if (i % 2 == 0) {
                        testWorkers[i] = new HourlyPaidWorker(workerID_test[i], workersName_test[i], new BigDecimal(rate_test[i], MathContext.DECIMAL64));
                    } else
                        testWorkers[i] = new FixedPaidWorker(workerID_test[i], workersName_test[i], new BigDecimal(rate_test[i], MathContext.DECIMAL64));
                }

                compareByName(testWorkers);
                compareBySalary(testWorkers);
                printWorkersByOrder_5_UpSide_3_DownSide(testWorkers);

                System.out.println("**********************************************************************");
            }

            System.out.println("End of program");
            }
    }

    /**
     * To compare workers by name
     *
     * @see
     */
    public static void compareByName(Worker[]worker){
        int n = worker.length;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (worker[j - 1].getName().compareTo(worker[j].getName()) > 0) {
                    Worker temp = worker[j - 1];
                    worker[j - 1] = worker[j];
                    worker[j] = temp;
                }
            }
        }
    }

    /**
     * To compare workers by salary
     *
     * @see
     */
    public static void compareBySalary(Worker[]worker) {
        int n = worker.length;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (worker[j - 1].monthlySalaryCalculation().compareTo(worker[j].monthlySalaryCalculation()) < 0) {
                    Worker temp = worker[j - 1];
                    worker[j - 1] = worker[j];
                    worker[j] = temp;
                }
            }
        }
    }

    /**
     * To print workers (in best scenario 5 workers up side and 3 workers down side
     *
     * @see
     */
    public static void printWorkersByOrder_5_UpSide_3_DownSide(Worker[]worker) {
        int counter = 0;

        for (counter = 0; counter < worker.length && counter < 5; counter++) {
            worker[counter].printWorker();
        }

        for (int i = worker.length - 3 > 0+counter ? worker.length - 3 : 0+counter;
             i < worker.length
                ; ++i) {
            worker[i].printWorker();
        }

    }

}


