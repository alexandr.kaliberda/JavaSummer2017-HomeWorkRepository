package JavaSummerHomeWork.SalaryPackage;

import java.math.BigDecimal;
import java.math.MathContext;


public class HourlyPaidWorker extends Worker {

    HourlyPaidWorker(int workerID, String name, BigDecimal rate){
        super(workerID,name,rate);
    }

    HourlyPaidWorker(int workerID, BigDecimal rate) {
        super(workerID,rate);
        setName("Unknown name");
    }

    HourlyPaidWorker() {
        setName("Unknown name");
    }

    /**
     * Used to calculate hourly salary workers.
     *
     * @see
     */
    @Override
    public BigDecimal monthlySalaryCalculation() {

        return new BigDecimal(20.8,MathContext.DECIMAL64)
                   .multiply(new BigDecimal(8,MathContext.DECIMAL64)
                           .multiply(getRate(),MathContext.DECIMAL64));
    }
}
