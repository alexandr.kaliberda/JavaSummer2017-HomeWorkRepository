package JavaSummerHomeWork.SalaryPackage;

import java.math.BigDecimal;

public abstract class Worker {
    private int workerID;
    private String name;
    private BigDecimal rate;

    Worker(int _workerID, BigDecimal _rate) {
        workerID=_workerID;
        name = "Unknown name";
        rate=_rate;
    }

    Worker(int _workerID, String _name, BigDecimal _rate){
        workerID=_workerID;
        name=_name;
        rate=_rate;
    }

    Worker() {
        name = "Unknown name";
        rate= BigDecimal.valueOf(0);
    }

    /**
     * The abstract method is used to override it in child classes -
     * - HourlyPaidWorker, FixedPaidWorker, and calculate
     * their salary by their own way.
     *
     * @see
     */
    public abstract BigDecimal monthlySalaryCalculation();


    public int getWorkerID() {
        return workerID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getRate() {
        return rate;
    }


    /**
     * To print given worker's personal data
     *
     * @see
     */
    public void printWorker(){
        System.out.printf("Worker ID: %-5d. Worker name: %-12s. Worker salary: %f.%n", getWorkerID(), getName(), monthlySalaryCalculation());
    }

}

