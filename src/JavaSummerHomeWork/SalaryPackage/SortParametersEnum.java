package JavaSummerHomeWork.SalaryPackage;

/**
 * Used to define how to show workers depend on their amount.
 * (MORE_THAN_OR_EQUAL_10_WORKERS) - will show 5 workers at up side and 3 workers at down side.
 * (LESS_THAN_10_AND_MORE_OR_EQUAL_TO_4_WORKERS) - will show 3 workers at up side and 3 workers at down side.
 * (LESS_OR_EQUAL_3_AND_MORE_OR_EQUAL_2_WORKERS) - will show 1 worker at up side and 1 worker at down side.
 * (EQUAL_TO_1_WORKER) - will show 1 worker at up side only.
 * (OTHER) - will show show warning message.
 *
 * @see
 */
public enum SortParametersEnum {
    MORE_THAN_OR_EQUAL_10_WORKERS, LESS_THAN_10_AND_MORE_OR_EQUAL_TO_4_WORKERS,
    LESS_OR_EQUAL_3_AND_MORE_OR_EQUAL_2_WORKERS, EQUAL_TO_1_WORKER, OTHER;

}
