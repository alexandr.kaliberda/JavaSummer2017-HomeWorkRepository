package JavaSummerHomeWork.SalaryPackage;

import java.math.BigDecimal;

public class FixedPaidWorker extends Worker {

    FixedPaidWorker(int workerId, String name, BigDecimal rate){
        super(workerId,name,rate);
    }

    FixedPaidWorker(int workerID, BigDecimal rate) {
        super(workerID,rate);
        setName("Unknown name");
    }

    FixedPaidWorker() {
        setName("Unknown name");
    }

    /**
     * Used to calculate fixed salary workers.
     *
     * @see
     */
    @Override
    public BigDecimal monthlySalaryCalculation() {
        return getRate();
    }
}
