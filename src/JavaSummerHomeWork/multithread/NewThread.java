package JavaSummerHomeWork.multithread;

class NewThread implements Runnable {

    Thread _t;
    String _name;

    NewThread(String name) {
        _t = new Thread(this, name);
        _name = name;
        _t.start();
    }

    @Override
    public void run() {
        System.out.println("Thread " + _name + " has been started.");
        try {
            System.out.println("From thread: " + _name + " ");
            Thread.sleep(99999);

        } catch (InterruptedException e) {
            System.out.println("Thread " + _name + " has been stopped\n");
        }
    }
}
