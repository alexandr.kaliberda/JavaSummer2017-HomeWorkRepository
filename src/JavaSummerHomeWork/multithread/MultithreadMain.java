package JavaSummerHomeWork.multithread;

import java.lang.*;
import java.util.Scanner;

/*
Задача на создание и остановку потоков
Программа должна запустить 5 потоков с именами Т1, Т2, Т3, Т4 и Т5.
Каждый поток при старте должен вывести на экран текст "Поток <имя потока> запустился"

После старта потоков приложение должно спросить пользователя, какой
поток он хочет остановить. Пользователь вводит имя потока. Если имя
введено верно и такой поток еще работает - это поток нужно остановить.
При остановке поток пишет в консоль "Поток <имя потока> остановлен".
Приложение завершается когда пользователь остановил таким образом все 5 потоков.
*/

public class MultithreadMain {
    public static void main(String[] args) throws InterruptedException {

        Scanner sc = new Scanner(System.in);

        NewThread nt1 = new NewThread("T1");
        NewThread nt2 = new NewThread("T2");
        NewThread nt3 = new NewThread("T3");
        NewThread nt4 = new NewThread("T4");
        NewThread nt5 = new NewThread("T5");

        int counter = 0;

        while (counter<5) {
            Thread.sleep(1000);
            System.out.println("Which thread would you like to stop ?");
            String choice = sc.nextLine();

            switch (choice){
                case "T1": {
                    if(nt1._t.isAlive()==true) {
                        counter++;
                        nt1._t.interrupt();
                    }
                }
                break;

                case "T2": {
                    if(nt2._t.isAlive()==true) {
                        counter++;
                        nt2._t.interrupt();
                    }
                }
                break;

                case "T3": {
                    if(nt3._t.isAlive()==true) {
                        counter++;
                        nt3._t.interrupt();
                    }
                }
                break;

                case "T4": {
                    if(nt4._t.isAlive()==true) {
                        counter++;
                        nt4._t.interrupt();
                    }
                }
                break;

                case "T5": {
                    if(nt5._t.isAlive()==true) {
                        counter++;
                        nt5._t.interrupt();
                    }
                }
                break;

                default:
                    System.err.println("Wrong thread name. Please try again.\n");
            }
        }
    }
}
