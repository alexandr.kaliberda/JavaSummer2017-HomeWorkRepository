package JavaSummerHomeWork.gumArrayException;

//HW_1504549217
//https://youtu.be/PhsYtG-fTvw
//Создать класс резинового массива, который умеет расширяться или сужаться по необходимости

//HW_1507573703
/*Видео с занятия: https://youtu.be/5PkbNKb0Rsg
Добавить обработку исключений для методов Вашего связного списка или резинового массива.
 Например, обрабатывать случаи выхода за границу коллекции, обращение к пустой коллекции и т.д.
 Создайте для этих целей свое собственное исключение.*/

import java.util.Arrays;
import java.util.Scanner;

public class GumArrayExcMain {
    public static void main(String[] args) {

        System.out.println("Test_1. TryToPrintEmptyArrayException exception\n");
        GumArrayExc g = new GumArrayExc();

        System.out.println("Attempt to print empty Array. TryToPrintEmptyArrayException expected");
        g.showElementsInArray();
        System.out.println("***********************************************************\n");

        System.out.println("\n Attempt to add 1 element to array and print the array.");
        g.addToArray(1);
        g.showElementsInArray();
        g.cleanArray();
        System.out.println("***********************************************************\n");


        System.out.println("Test_2. TryToAddZeroElToArrayException exception");
        System.out.println("Attempt to add no elements to empty array.");
        int[] arr1=new int[0];

        try {
            g.addToArray(arr1);
        }catch (TryToAddZeroElToArrayException e){
            System.out.println(e);
        }
        g.cleanArray();
        g.showElementsInArray();
        System.out.println("***********************************************************\n");


        //OLD code starts from here

//        Scanner sc = new Scanner(System.in);
//
//        GumArrayExc gumArrayExc = new GumArrayExc();
//        System.out.println("Test of gum array will be performed.\nPlease write 1 to show the test, or any key to exit");
//
//        //In this part, checking user's choice.
//        //1 to look at gum array tests, otherwise any button to exit the program
//        int choice;
//        if(!sc.hasNextInt())
//            choice=2;
//        else choice = sc.nextInt();
//
//        if(choice==1){
//
//            //Test part start's from here
//            {
//                System.out.println("Test of getGumArray() getter will be performed");
//                int[] testedArray = {1, 2, 3};
//                boolean result = true;
//
//                gumArrayExc.addToArray(testedArray);
//                int[] expectedResultsArray = gumArrayExc.getGumArray();
//                for (int i = 0; i < testedArray.length; i++) {
//                    if (expectedResultsArray[i] != testedArray[i]) result = false;
//                }
//                if(!result) System.err.println("Test was not passed.");
//                else System.out.println("Test passed.");
//                gumArrayExc.cleanArray();
//            }
//
//            System.out.println("*******************************************************************");
//
//            {
//                System.out.println("Test of overloaded addToArray(int[]) method will be performed");
//                int[] testedArray = {1, 2, 3};
//                boolean result = true;
//
//                gumArrayExc.addToArray(testedArray);
//                int[] expectedResultsArray = gumArrayExc.getGumArray();
//                for (int i = 0; i < testedArray.length; i++) {
//                    if (expectedResultsArray[i] != testedArray[i]) result = false;
//                }
//                if(!result) System.err.println("Test was not passed.");
//                else System.out.println("Test passed.");
//                gumArrayExc.cleanArray();
//            }
//
//            System.out.println("*******************************************************************");
//
//            {
//                System.out.println("Test of overloaded addToArray(int) method will be performed");
//                int[] testedValue = {1,2,3,4,5,6,7,8,9,10,11};
//                boolean result = true;
//
//                for (int i = 0; i < testedValue.length; i++) {
//                    gumArrayExc.addToArray(testedValue[i]);
//                }
//                int[] expectedResult = {1,2,3,4,5,6,7,8,9,10,11,0,0,0,0,0,0,0,0,0};
//                for (int i = 0; i < expectedResult.length; i++) {
//                    if (expectedResult[i]!= gumArrayExc.getGumArray()[i])result = false;
//                }
//                if(!result) System.err.println("Test was not passed.");
//                else System.out.println("Test passed.");
//            }
//
//            System.out.println("*******************************************************************");
//
//            {
//                System.out.println("Test of dropElementsFromLeftSide() method will be performed");
//                int[] testedArray = {1,2,3,4};
//                boolean result = true;
//
//                int[] expectedResultsArray = {2,3,4};
//                gumArrayExc.addToArray(testedArray);
//                gumArrayExc.dropElementsFromLeftSide(1);
//
//                for (int i = 0; i < expectedResultsArray.length; i++) {
//                    if (expectedResultsArray[i] != gumArrayExc.getGumArray()[i]) result = false;
//                }
//                if(!result) System.err.println("Test was not passed.");
//                else System.out.println("Test passed.");
//                gumArrayExc.cleanArray();
//            }
//
//            System.out.println("*******************************************************************");
//
//            {
//                System.out.println("Test of dropElementsFromRightSide() method will be performed");
//                int[] testedArray = {1,2,3,4};
//                boolean result = true;
//
//                int[] expectedResultsArray = {1,2,3};
//                gumArrayExc.addToArray(testedArray);
//                gumArrayExc.dropElementsFromRightSide(1);
//
//                for (int i = 0; i < expectedResultsArray.length; i++) {
//                    if (expectedResultsArray[i] != gumArrayExc.getGumArray()[i]) result = false;
//                }
//                if(!result) System.err.println("Test was not passed.");
//                else System.out.println("Test passed.");
//                gumArrayExc.cleanArray();
//            }
//
//            System.out.println("*******************************************************************");
//
//            {
//                System.out.println("Test of showElementsInArray() method will be performed.\n" +
//                        "Visual comparation only.");
//                int[] testedArray = {1,2,3,4};
//
//                System.out.println(Arrays.toString(testedArray));
//                gumArrayExc.addToArray(testedArray);
//                gumArrayExc.showElementsInArray();
//            }
//
//            System.out.println("\n*******************************************************************");
//
//            {
//                System.out.println("Test of cleanArray() method will be performed.\n" +
//                        "Visual comparation only. Expected no values in array.");
//                int[]testedArray={1,2,3};
//                gumArrayExc.addToArray(testedArray);
//                gumArrayExc.cleanArray();
//                gumArrayExc.showElementsInArray();
//            }
//        }
//        System.out.println("End of program");
    }
}
