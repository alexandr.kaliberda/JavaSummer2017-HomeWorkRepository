package JavaSummerHomeWork.carDataBase_HW_1508350940.db;

import JavaSummerHomeWork.carDataBase_HW_1508350940.entities.Car;
import JavaSummerHomeWork.carDataBase_HW_1508350940.entities.CarNumber;
import java.util.Map;

public interface DataBase {
    Map<CarNumber,Car>findAll();
    Car findByNumber (CarNumber carNumber);
    OperationResult add(CarNumber carNumber,Car car);
    OperationResult delete(CarNumber carNumber);
    OperationResult delete(Car car);
    OperationResult deleteAll();
}
