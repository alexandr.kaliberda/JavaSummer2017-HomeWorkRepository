package JavaSummerHomeWork.carDataBase_HW_1508350940.db.impl.memory;

import JavaSummerHomeWork.carDataBase_HW_1508350940.db.DataBase;
import JavaSummerHomeWork.carDataBase_HW_1508350940.db.OperationResult;
import JavaSummerHomeWork.carDataBase_HW_1508350940.entities.Car;
import JavaSummerHomeWork.carDataBase_HW_1508350940.entities.CarNumber;

import java.util.*;

public class MemoryDataBase implements DataBase{

    Map<CarNumber,Car>_data;

    public MemoryDataBase(){
        _data=new HashMap<>();
    }

    @Override
    public Map<CarNumber, Car> findAll() {

        return Collections.unmodifiableMap(_data);
    }

    @Override
    public Car findByNumber(CarNumber carNumber) {
        Car modifiableCar = _data.get(carNumber);
        return Car.unmodifiable(modifiableCar);
    }

    @Override
    public OperationResult add(CarNumber carNumber, Car car) {

        Car result = _data.putIfAbsent(carNumber,car);
        if(result==null){
            return OperationResult.SUCCESS;
        }
        return OperationResult.EXIST;
    }

    @Override
    public OperationResult delete(CarNumber carNumber) {
        Car removedCar = _data.remove(carNumber);
        if(removedCar==null) {
            return OperationResult.NOT_EXIST;
        }
        return OperationResult.SUCCESS;
    }

    @Override
    public OperationResult delete(Car car) {

        Set<CarNumber>carNumbers = new HashSet<>();

        for(Map.Entry<CarNumber,Car> entry: _data.entrySet()) {

            if(Objects.equals(entry.getValue(),car)){
                carNumbers.add(entry.getKey());
            }
        }

        for(CarNumber carNumber: carNumbers){
            _data.remove(carNumber);
        }

        return carNumbers.size()==0?
                OperationResult.NOT_EXIST:
                OperationResult.SUCCESS;
    }



    @Override
    public OperationResult deleteAll() {
        return null;
    }
}