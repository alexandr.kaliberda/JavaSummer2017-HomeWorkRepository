package JavaSummerHomeWork.carDataBase_HW_1508350940;

import JavaSummerHomeWork.carDataBase_HW_1508350940.db.DataBase;
import JavaSummerHomeWork.carDataBase_HW_1508350940.db.impl.memory.MemoryDataBase;
import JavaSummerHomeWork.carDataBase_HW_1508350940.entities.Car;
import JavaSummerHomeWork.carDataBase_HW_1508350940.entities.Driver;

import java.awt.*;
import java.util.GregorianCalendar;

/*
CarDataBase_HW_1508350940
https://youtu.be/53WOPU78GGU
Дописать класс Driver из классного проекта. Реализовать клонируемость всех сущностей.
Законспектировать теорию по интерфейсу Map и его реализациям.
*/
public class Main {

    public static void main(String[] args) {

        //CarDataBase_HW_1508350940 cloning Driver
        System.out.println("Test of Driver cloning");

        Driver driver1 = new Driver("Vasya","Pupkin");
        Driver driver2=new Driver();

        try{
            driver2=driver1.clone();
        }catch (CloneNotSupportedException e){
            e.printStackTrace();
        }

        driver2.set_firstName("Petya");

        System.out.println(driver1.get_firstName());
        System.out.println(driver2.get_firstName());

        ///////////////////////////////////////////////////////////////////////////

        //CarDataBase_HW_1508350940 cloning Car
        DataBase dataBase = new MemoryDataBase();
        Car car = new Car("model1","mark1", new GregorianCalendar(2014,9,10),Color.BLACK);
        Car car1 = new Car("","", new GregorianCalendar(0,0,0),Color.BLACK);
        try {
            car1=car.clone();
        }catch (CloneNotSupportedException e){
            e.printStackTrace();
        }

        car1.set_color(Color.GRAY);
        System.out.println(car.get_color());
        System.out.println(car1.get_color());

        ///////////////////////////////////////////////////////////////////////////

        //CarDataBase_HW_1508350940 cloning CarNumber is not required as
        //CarNumber is not mutable
    }
}
