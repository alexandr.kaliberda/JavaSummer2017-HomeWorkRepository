package JavaSummerHomeWork.carDataBase_HW_1508350940.entities;

import java.lang.IllegalArgumentException;

public class CarNumber implements Cloneable{

    private final String _serialBeforeNumber;
    private final int _number;
    private final String _serialAfterNumber;

    public CarNumber(final String serialBeforeNumber,
                     final int number,
                     final String serialAfterNumber) {
        checkArgs(serialBeforeNumber,number,serialAfterNumber);
        _serialBeforeNumber = serialBeforeNumber;
        _number = number;
        _serialAfterNumber = serialAfterNumber;
    }

    private void checkArgs(final String serialBeforeNumber,
                           final int number,
                           final String serialAfterNumber) {
        if(serialBeforeNumber==null
                || serialAfterNumber == null
                || number > 9999
                || number < 0) {
            throw new IllegalArgumentException(String.format("Invalid args %s %d %s%n",
                    serialBeforeNumber, number, serialAfterNumber));
        }
    }

    public String get_serialBeforeNumber() {
        return _serialBeforeNumber;
    }

    public int get_number() {
        return _number;
    }

    public String get_serialAfterNumber() {
        return _serialAfterNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarNumber carNumber = (CarNumber) o;

        if (get_number() != carNumber.get_number()) return false;
        if (!get_serialBeforeNumber().equals(carNumber.get_serialBeforeNumber())) return false;
        return get_serialAfterNumber().equals(carNumber.get_serialAfterNumber());
    }

    @Override
    public int hashCode() {
        int result = get_serialBeforeNumber().hashCode();
        result = 31 * result + get_number();
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s %.4d %s", get_serialBeforeNumber(),get_number(),get_serialAfterNumber());
    }

    public CarNumber clone()throws CloneNotSupportedException{
        return (CarNumber)super.clone();
    }
}