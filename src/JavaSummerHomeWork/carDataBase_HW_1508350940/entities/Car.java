package JavaSummerHomeWork.carDataBase_HW_1508350940.entities;

import java.awt.*;
import java.util.*;

public class Car implements Cloneable{
    private final String _model;
    private final String _mark;
    private Calendar _year;

    private Set<Driver>_drivers=new HashSet<>();
    private int distance;
    private Color _color;

    public Car(String model, String mark, Calendar year, Color color) {

        if(model==null||mark==null||year==null||color==null){
            throw new IllegalArgumentException("Error creating car. Argument is null");
        }
        this._model = model;
        this._mark = mark;
        this._year = year;
        this._color = color;
    }


    private Car(Car car){
        _color=car._color;
        _model=car._model;
        _mark=car._mark;
        distance=car.distance;
        _drivers=car._drivers;
        _year=car._year;
    }

    public void setDistance(int distance) {
        if(distance<0)throw new IllegalArgumentException("Distance is < 0");
        this.distance = distance;
    }

    public void set_color(Color color) {
        if(_color==null)throw new NullPointerException("Color = null");
        this._color = color;
    }

    public String get_model() {
        return _model;
    }

    public String get_mark() {
        return _mark;
    }

    public Set<Driver> get_drivers() {
        return Collections.unmodifiableSet(_drivers);
    }

    public Calendar get_year() {
        return _year;
    }

    public int getDistance() {
        return distance;
    }

    public Color get_color() {
        return _color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (getDistance() != car.getDistance()) return false;
        if (!get_model().equals(car.get_model())) return false;
        if (!get_mark().equals(car.get_mark())) return false;
        if (!get_drivers().equals(car.get_drivers())) return false;
        if (!get_year().equals(car.get_year())) return false;
        return get_color().equals(car.get_color());
    }

    @Override
    public int hashCode() {
        int result = get_model().hashCode();
        result = 31 * result + get_mark().hashCode();
        result = 31 * result + get_year().hashCode();
        return result;
    }

    public Car clone()throws CloneNotSupportedException{
        Car c = (Car)super.clone();
        c._year=(GregorianCalendar)_year.clone();
        return c;
    }

    public boolean isModifiable(){
        return false;
    }

    public static Car unmodifiable(Car car){
        return new UnmodifiableCar(car);
    }



////////////////////////////////////////////////////////////////////////////////////////////////

    private static class UnmodifiableCar extends Car{

        public UnmodifiableCar(Car car) {
            super(car);
        }

        @Override
        public void setDistance(int distance) {
            throw new UnsupportedOperationException("Car is not modifiable");
        }

        @Override
        public void set_color(Color color) {
            throw new UnsupportedOperationException("Car is not modifiable");
        }

        public boolean isModifiable(){
            return true;
        }
    }
}