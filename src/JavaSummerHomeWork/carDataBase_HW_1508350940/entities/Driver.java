package JavaSummerHomeWork.carDataBase_HW_1508350940.entities;

public class Driver implements Cloneable{

    private String _firstName, _lastName;

    public Driver(String firstName,String lastName){
        checkArgs(firstName,lastName);
        _firstName=firstName;
        _lastName=lastName;
    }

    public Driver(){};

    private void checkArgs(String firstName,
                           String lastName) {
        if(firstName==null
                || lastName == null) {
            throw new IllegalArgumentException(String.format("Invalid args %s %s%n",
                    firstName, lastName));
        }
    }

    public Driver clone() throws CloneNotSupportedException{
        return (Driver)super.clone();
    }

    public String get_firstName() {
        return _firstName;
    }

    public void set_firstName(String _firstName) {
        this._firstName = _firstName;
    }

    public String get_lastName() {
        return _lastName;
    }

    public void set_lastName(String _lastName) {
        this._lastName = _lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Driver driver = (Driver) o;

        if (!get_firstName().equals(driver.get_firstName())) return false;
        return get_lastName().equals(driver.get_lastName());
    }

    @Override
    public int hashCode() {
        return 0;
    }
}