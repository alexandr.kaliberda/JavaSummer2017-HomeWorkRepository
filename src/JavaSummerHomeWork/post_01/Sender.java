package JavaSummerHomeWork.post_01;

import java.util.Random;

/*
Создать класс Sender, задача которого - постоянно
приносить новые посылки на почту. Он делает это с рандомной
регулярностью. Если на почте нет места, то он ждет появления места.
 */
public class Sender extends Thread {
    Random random = new Random();
    private Post _post;
    private String _senderName;
    private boolean threadSleep = false;

    Sender(Post post, String senderName) {
        _post = post;
        _senderName = senderName;
    }

    //Amount of posts that sender able to send at one time.
    private final int postFromSender = 1;

    public void run() {
        while (threadSleep == false) {

            //Random sleep generation.
            int x;
            x = random.nextInt(500) + 500;

            try {
                if (_post.hasSpaceForPost() == true) {
                    System.out.println(_senderName + " Put post to box.\n");
                    putToPost(postFromSender);
                    sleep(x);

                } else {
                    sleep(x);
                    System.err.println(getSenderName() + ": Post box has been overloaded, wait for a while.\n");
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * The method has connection with class Post, and calls
     * Post's 'getFromSender' method, so that post received
     * from sender by the method.
     *
     * @param postFromSender the value that symbolizes (physical) post from sender.
     */
    private synchronized void putToPost(int postFromSender) {
        _post.getFromSender(postFromSender);
    }

    public String getSenderName() {
        return _senderName;
    }

    /**
     * The method stops sender's thread by changing boolean value to 'false'.
     */
    public void stopThread() {
        threadSleep = true;
    }
}

