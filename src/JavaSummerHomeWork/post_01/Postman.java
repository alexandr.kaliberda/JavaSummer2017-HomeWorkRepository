package JavaSummerHomeWork.post_01;

/*
Создать класс Postman, почтальон, задача которого - забирать
посылки с фиксированной регулярностью. Если посылок на почте нет - ожидать.
У почтальона может быть фиксированная грузоподъемность (то есть за
раз забрать несколько посылок).
 */

public class Postman extends Thread {
    private Post _post;
    private String _postManName;
    private int _abltyToCollect;
    private boolean isStopped;

    Postman(Post post, String postmanName, int abltyToCllect) {
        _post = post;
        _postManName = postmanName;
        _abltyToCollect = abltyToCllect;
    }

    public void run() {
        while (!isStopped) {

            //code that defines way to collect post
            try {
                if (_post.postBoxCounter() >= _abltyToCollect) {
                    collPost(_abltyToCollect);
                }

                if (_post.isEmpty()) {
                    System.out.println(getPostmanName() + ": No post, wait.\n");
                }

                sleep(500);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * The method gets
     *
     * @param abltyToCllect
     */
    public synchronized void collPost(int abltyToCllect) {
        _post.giveToPostman(abltyToCllect);
    }

    public String getPostmanName() {
        return _postManName;
    }

    /**
     * The method stops sender's thread by changing boolean value to 'true'
     */
    public void stopThread() {
        isStopped = true;
    }
}
