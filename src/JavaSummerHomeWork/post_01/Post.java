package JavaSummerHomeWork.post_01;
/*
Создать класс Почта, которая хранит посылки. Количество посылок,
одновременно лежащих на почте, ограничено.
 */

public class Post {
    private final int POSTBUFFER = 10;
    private final Object key = new Object();

    //Counters of total amount posts ever received
    private int totalReceivedPosts;
    private int totalSentPosts;

    //Post box. Senders puts posts inside of postBoxCounter
    //Post box can store limited amount of posts <= POSTBUFFER
    private int postBoxCounter;


    /**
     * The method gets posts from senders and puts it to post box
     *
     * @param postFromSender the value receive from Sender class and symbolizes (physical) post from sender.
     */
    public void getFromSender(int postFromSender) {
        synchronized (key) {
            postBoxCounter += postFromSender;
            System.out.println("Post: post amount is: " + postBoxCounter() + "\n");
            totalReceivedPosts++;
        }
    }

    public void giveToPostman(int abltyToCllect) {
        synchronized (key) {
            int _abltyToCollect = abltyToCllect;
            postBoxCounter -= abltyToCllect;
            System.out.println(_abltyToCollect + " post/s collected. remain: " + postBoxCounter() + "\n");
            totalSentPosts += _abltyToCollect;
        }
    }

    /**
     * The method checking whether post box occupied or not.
     *
     * @return
     */
    public boolean hasSpaceForPost() {
        return postBoxCounter < POSTBUFFER;
    }

    /**
     * The method checking whether post box empty or not.
     *
     * @return
     */
    public boolean isEmpty() {
        return postBoxCounter <= 0;
    }

    public int postBoxCounter() {
        return postBoxCounter;
    }

    public int totalReceivedCounter() {
        return totalReceivedPosts;
    }

    public int totalSentCounter() {
        return totalSentPosts;
    }

}
