package JavaSummerHomeWork.post_01;
/*
Количество Sender и Postman может быть разным и задается в константах.

Sender и Postman являются потоками, которые должны быть запущены
и работать в течении определенного времени. По истечении времени нужно
остановить эти потоки и проверить, что количество отосланных
посылок = количество доставленных + остаток на почте.
*/

public class PostMain {

    /**
     * AMOUNT_OF_SENDERS is amount of senders that going to send their posts
     */
    private static final int AMOUNT_OF_SENDERS = 5;

    /**
     * AMOUNT_OF_POSTMANS is amount of post employees
     */
    private static final int AMOUNT_OF_POSTMANS = 1;

    /**
     * POST_CAN_BE_CARED_BY_ONE_POSTMAN is amount of posts that can be cared by one postman
     */
    private static final int POST_CAN_BE_CARED_BY_ONE_POSTMAN = 2;

    public static void main(String[] args) {

        Post p = new Post();
        Sender[] sender = new Sender[AMOUNT_OF_SENDERS];
        Postman[] postman = new Postman[AMOUNT_OF_POSTMANS];

        //Initialization of Sender/s
        for (int i = 0; i < sender.length; i++) {
            sender[i] = new Sender(p, "Sender_" + i);
            sender[i].start();
        }

        //Initialization of Postman/s
        for (int i = 0; i < postman.length; i++) {
            postman[i] = new Postman(p, "Postman_" + i, POST_CAN_BE_CARED_BY_ONE_POSTMAN);
            postman[i].start();
        }


        ///////////////THREAD SLEEPING AND STOPPING PART///////////////////////
        try {
            System.err.println("Main thread sleep");
            Thread.sleep(4000);
            System.err.println("Main thread woke up");


            //Stopping threads sender
            for (int i = 0; i < sender.length; i++) {
                System.out.println(sender[i].getSenderName() + " stopped");
                sender[i].stopThread();
            }

            //Stopping threads postman
            for (int i = 0; i < postman.length; i++) {
                System.out.println(postman[i].getPostmanName() + " stopped");
                postman[i].stopThread();

                System.out.println();
            }


            //Summarising amount of posts
            System.out.printf("Total received posts: %d%n", p.totalReceivedCounter());
            System.out.printf("Total sent posts: %d%n", p.totalSentCounter());
            System.out.printf("Total Remain on post: %d%n%n", p.postBoxCounter());

        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        //join() part
        try {
            for (int i = 0; i < sender.length; i++) {
                sender[i].join();
            }

            for (int i = 0; i < postman.length; i++) {
                postman[i].join();
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
