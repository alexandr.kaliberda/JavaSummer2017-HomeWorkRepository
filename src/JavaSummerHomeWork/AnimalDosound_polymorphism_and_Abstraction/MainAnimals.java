package JavaSummerHomeWork.AnimalDosound_polymorphism_and_Abstraction;

//HW_1505154369
//Реализовать абстрактный класс Animal с абстрактным методом doSound()
// - звук конкретного животного. Отнаследовать от него 3 класса
// конкретных животных и реализовать абстрактный метод. Создать
// массив различных животных и в цикле вызвать у каждого doSound()

public class MainAnimals {
    public static void main(String[] args) {

        Animals[] arrayOfAnimals = new Animals[3];//Create Animal's type array
        arrayOfAnimals[0] = new Mouse();//assign Mouse object to common array (Animal)
        arrayOfAnimals[1] = new Cat();//assign Cat object to common array (Animal)
        arrayOfAnimals[2] = new Dog();//assign Dog object to common array (Animal)

        //Do loop and call Mouse's, Cat's, Dog's own redefined doSound() method
        for (int i = 0; i < arrayOfAnimals.length; i++) {
            arrayOfAnimals[i].doSound();
        }

    }
}
