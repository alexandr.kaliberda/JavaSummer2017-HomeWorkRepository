package JavaSummerHomeWork.AnimalDosound_polymorphism_and_Abstraction;

abstract public class Animals {

    //Abstract method doSound. Child classes to redefine the method by their own way
    public abstract void doSound();

}
