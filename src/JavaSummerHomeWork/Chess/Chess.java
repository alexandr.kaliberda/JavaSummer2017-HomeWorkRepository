package JavaSummerHomeWork.Chess;

public class Chess {
    //HW_1503340195
//  Поле шахматной доски определяется парой натуральных чисел, каждое из ко-
//  торых не превосходит 8: первое число — номер вертикали (при счете слева
//  направо), второе — номер горизонтали (при счете снизу вверх). Даны нату-
//  ральные числа a, b, c, d, каждое из которых не превосходит 8.
//
//  а) На поле (a, b) расположена ладья. Записать условие, при котором она угро-
//  жает полю (c, d).
//
//  б) На поле (a, b) расположен слон. Записать условие, при котором он угрожает
//  полю (c, d).
//
//  в) На поле (a, b) расположен король. Записать условие, при котором он может
//  одним ходом попасть на поле (c, d).
//
//  г) На поле (a, b) расположен ферзь. Записать условие, при котором он угрожа-
//  ет полю (c, d).
//
//  д) На поле (a, b) расположена белая пешка. Записать условие, при котором
//  она может одним ходом попасть на поле (c, d):
//  при обычном ходе;
//  когда она "бьет" фигуру или пешку соперника.
//
//  Примечание
//  Белые пешки перемещаются на доске снизу вверх.
//
//  е) На поле (a, b) расположена черная пешка. Записать условие, при котором
//  она может одним ходом попасть на поле (c, d):
//  при обычном ходе;
//  когда она "бьет" фигуру или пешку соперника.
//  Примечание
//  Черные пешки перемещаются на доске сверху вниз.
//
//  ж) На поле (a, b) расположен конь. Записать условие, при котором он угрожа-
//  ет полю (c, d).

    public static void main(String[] args) {

        int[][] chessField = new int[8][8];//Setup field size
        int xABFigure = 3, yABFigure = 6;//AB figure X and Y location
        int xCDEnemyFigure = 2, yCDEnemyFigure = 6;//CD figure X and Y location
        boolean threatToBCfigure = false;

        chessField[xABFigure][yABFigure] = 2;//put AB figure (named 2) to desired location
        chessField[xCDEnemyFigure][yCDEnemyFigure] = 1;//put CD enemy figure (named 1) to desired location

        System.out.println("Rook movement");
        chessPrint_Of_Threat(rookMovement(xABFigure, yABFigure, chessField.length, chessField));
        System.out.println("************************************");

        System.out.println("Bishop movement");
        chessPrint_Of_Threat(bishopMovement(xABFigure, yABFigure, chessField.length, chessField));
        System.out.println("************************************");

        System.out.println("King movement");
        chessPrint_Of_Threat(kingMovement(xABFigure, yABFigure, chessField.length, chessField));
        System.out.println("************************************");

        System.out.println("Queen movement");
        chessPrint_Of_Threat(queenMovement(xABFigure, yABFigure, chessField.length, chessField));
        System.out.println("************************************");

        System.out.println("White pawn movement");
        chessPrint_Of_Threat(whitePawnMovement(xABFigure, yABFigure, chessField.length, chessField));
        System.out.println("************************************");

        System.out.println("Black pawn movement");
        chessPrint_Of_Threat(blackPawnMovement(xABFigure, yABFigure, chessField.length, chessField));
        System.out.println("************************************");

        System.out.println("Knight movement");
        chessPrint_Of_Threat(knightMovement(xABFigure, yABFigure, chessField.length, chessField));
        System.out.println("************************************");

        //Test Part
        System.out.println("TEST TEST TEST TEST TEST TEST TEST TEST TEST ");

        testRook();
        testBishop();
        testKing();
        testQueen();
        testWhitePawn();
        testBlackPawn();
        testKnight();
    }

    private static boolean rookMovement(int x, int y, int chessFieldLength, int[][] chessField) {

        int xUp = x;
        int xDown = x;
        int yUp = y;
        int yDown = y;
        int flag = 0;

        boolean threatSignal = false;

        for (int i = 0; i < 8; i++) {

            xUp++;//movement of x from figure location to right side
            if (xUp < chessFieldLength)
                if (threatSignal = analyzeAndAction(chessField, xUp, y)) flag++;

            xDown--;//movement of x from figure location to left side
            if (xDown >= 0)
                if (threatSignal = analyzeAndAction(chessField, xDown, y)) flag++;

            yUp++;
            if (yUp < chessFieldLength)
                if (threatSignal = analyzeAndAction(chessField, x, yUp)) flag++;

            yDown--;
            if (yDown >= 0)
                if (threatSignal = analyzeAndAction(chessField, x, yDown)) flag++;
        }

        showChessField(chessField);
        cleanChessBoard(chessField);
        if (flag > 0) return threatSignal = true;
        return threatSignal;
    }

    private static boolean bishopMovement(int x, int y, int chessFieldLength, int[][] chessField) {

        int xUp = x;
        int yUp = y;

        int xDown = x;
        int yDown = y;
        int flag = 0;

        boolean threatSignal = false;

        for (int i = 0; i < chessFieldLength; i++) {

            //movement to up side
            ++xUp;
            ++yUp;

            //movement to down side
            --xDown;
            --yDown;

            //movement to up right side
            if (xUp < chessFieldLength && yUp < chessFieldLength)
                if (threatSignal = analyzeAndAction(chessField, xUp, yUp)) flag++;

            //movement to right down side
            if (xUp < chessFieldLength && yDown >= 0)
                if (threatSignal = analyzeAndAction(chessField, xUp, yDown)) flag++;

            //movement to left up side
            if (xDown >= 0 && yUp < chessFieldLength)
                if (threatSignal = analyzeAndAction(chessField, xDown, yUp)) flag++;

            //movement to left down side
            if (xDown >= 0 && yDown >= 0)
                if (threatSignal = analyzeAndAction(chessField, xDown, yDown)) flag++;
        }
        showChessField(chessField);
        cleanChessBoard(chessField);
        if (flag > 0) return threatSignal = true;
        return threatSignal;
    }

    private static boolean kingMovement(int x, int y, int chessFieldLength, int[][] chessField) {

        int xUp = x;
        int yUp = y;

        int xDown = x;
        int yDown = y;
        int flag = 0;

        boolean threatSignal = false;

        for (int i = 0; i < chessFieldLength; i++) {

            //movement to up side
            ++xUp;

            //upper row
            if (xUp - 2 >= 0 && y + 1 < chessFieldLength && xUp - 2 < x + 2 && xUp - 2 < chessFieldLength)
                if (threatSignal = analyzeAndAction(chessField, xUp - 2, y + 1)) flag++;

            //middle row
            if (xUp - 2 >= 0 && y < chessFieldLength && xUp - 2 < x + 2 && xUp - 2 < chessFieldLength)
                if (threatSignal = analyzeAndAction(chessField, xUp - 2, y)) flag++;

            //lower row
            if (xUp - 2 >= 0 && y - 1 >= 0 && xUp - 2 < x + 2 && xUp - 2 < chessFieldLength)
                if (threatSignal = analyzeAndAction(chessField, xUp - 2, y - 1)) flag++;
        }
        showChessField(chessField);
        cleanChessBoard(chessField);
        if (flag > 0) return threatSignal = true;
        return threatSignal;
    }

    private static boolean queenMovement(int x, int y, int chessFieldLength, int[][] chessField) {

        int xUp = x;
        int xDown = x;
        int yUp = y;
        int yDown = y;
        int flag = 0;

        boolean threatSignal = false;

        for (int i = 0; i < 8; i++) {

            //movement to up side
            ++xUp;
            ++yUp;

            //movement to down side
            --xDown;
            --yDown;

            //taken from rook's logic
            if (xUp < chessFieldLength)
                if (threatSignal = analyzeAndAction(chessField, xUp, y)) flag++;

            if (xDown >= 0)
                if (threatSignal = analyzeAndAction(chessField, xDown, y)) flag++;

            if (yUp < chessFieldLength)
                if (threatSignal = analyzeAndAction(chessField, x, yUp)) flag++;

            if (yDown >= 0)
                if (threatSignal = analyzeAndAction(chessField, x, yDown)) flag++;

            //taken from bishop's logic
            //movement to up right side
            if (xUp < chessFieldLength && yUp < chessFieldLength)
                if (threatSignal = analyzeAndAction(chessField, xUp, yUp)) flag++;

            //movement to right down side
            if (xUp < chessFieldLength && yDown >= 0)
                if (threatSignal = analyzeAndAction(chessField, xUp, yDown)) flag++;

            //movement to left up side
            if (xDown >= 0 && yUp < chessFieldLength)
                if (threatSignal = analyzeAndAction(chessField, xDown, yUp)) flag++;

            //movement to left down side
            if (xDown >= 0 && yDown >= 0)
                if (threatSignal = analyzeAndAction(chessField, xDown, yDown)) flag++;
        }

        showChessField(chessField);
        cleanChessBoard(chessField);
        if (flag > 0) return threatSignal = true;
        return threatSignal;
    }

    private static boolean whitePawnMovement(int x, int y, int chessFieldLength, int[][] chessField) {

        int yUp = y;
        int flag = 0;
        boolean threatSignal = false;

        for (int i = 0; i < 2; i++) {

            //movement to up side
            ++yUp;

            //movement from 1st row
            if (y == 1 && yUp < y + 3 && chessField[x][yUp] != 1)
                if (threatSignal = analyzeAndAction(chessField, x, yUp)) flag++;

            //ordinary movement up
            if (y > 1 && yUp < y + 2 && yUp < chessFieldLength && chessField[x][yUp] != 1)
                if (threatSignal = analyzeAndAction(chessField, x, yUp)) flag++;

            //left threat
            if (x - 1 >= 0 && yUp < chessFieldLength && yUp < y + 2)
                if (threatSignal = analyzeAndAction(chessField, x - 1, yUp)) flag++;

            //right threat
            if (x + 1 < chessFieldLength && yUp < chessFieldLength && yUp < y + 2)
                if (threatSignal = analyzeAndAction(chessField, x + 1, yUp)) flag++;

            if (y == 0) System.out.println("Pawn cannot start from 0 row");
        }
        showChessField(chessField);
        cleanChessBoard(chessField);
        if (flag > 0) return threatSignal = true;
        return threatSignal;
    }

    private static boolean blackPawnMovement(int x, int y, int chessFieldLength, int[][] chessField) {

        int yDown = y;
        int flag = 0;
        boolean threatSignal = false;

        for (int i = 0; i < 2; i++) {

            //movement to up side
            --yDown;

            //movement from 1st row
            if (y == 6 && yDown > y - 3 && chessField[x][yDown] != 1 && chessField[x][yDown + 1] != 1)
                if (threatSignal = analyzeAndAction(chessField, x, yDown)) flag++;

            //ordinary movement down
            if (y < 6 && yDown > y - 2 && yDown >= 0 && chessField[x][yDown] != 1)
                if (threatSignal = analyzeAndAction(chessField, x, yDown)) flag++;

            //left threat
            if (x - 1 >= 0 && yDown >= 0 && yDown > y - 2)
                if (threatSignal = analyzeAndAction(chessField, x - 1, yDown)) flag++;

            //right threat
            if (x + 1 < chessFieldLength && yDown >= 0 && yDown > y - 2)
                if (threatSignal = analyzeAndAction(chessField, x + 1, yDown)) flag++;

            if (y == 7) System.out.println("Pawn cannot start from 0 row");
        }
        showChessField(chessField);
        cleanChessBoard(chessField);
        if (flag > 0) return threatSignal = true;
        return threatSignal;
    }

    private static boolean knightMovement(int x, int y, int chessFieldLength, int[][] chessField) {
        int flag = 0;
        boolean threatSignal = false;

        for (int i = 0; i < 1; i++) {

            //up left lower
            if (x - 2 >= 0 && y + 1 < chessFieldLength)
                if (threatSignal = analyzeAndAction(chessField, x - 2, y + 1)) flag++;

            //up left upper
            if (x - 1 >= 0 && y + 2 < chessFieldLength)
                if (threatSignal = analyzeAndAction(chessField, x - 1, y + 2)) flag++;

            //up right upper
            if (x + 1 < chessFieldLength && y + 2 < chessFieldLength)
                if (threatSignal = analyzeAndAction(chessField, x + 1, y + 2)) flag++;

            //up right lower
            if (x + 2 < chessFieldLength && y + 1 < chessFieldLength)
                if (threatSignal = analyzeAndAction(chessField, x + 2, y + 1)) flag++;

            //down right upper
            if (x + 2 < chessFieldLength && y - 1 >= 0)
                if (threatSignal = analyzeAndAction(chessField, x + 2, y - 1)) flag++;

            //down right lower
            if (x + 1 < chessFieldLength && y - 2 >= 0)
                if (threatSignal = analyzeAndAction(chessField, x + 1, y - 2)) flag++;

            //down left lower
            if (x - 1 >= 0 && y - 2 >= 0)
                if (threatSignal = analyzeAndAction(chessField, x - 1, y - 2)) flag++;

            //down left upper
            if (x - 2 >= 0 && y - 1 >= 0)
                if (threatSignal = analyzeAndAction(chessField, x - 2, y - 1)) flag++;
        }
        showChessField(chessField);
        cleanChessBoard(chessField);
        if (flag > 0) return threatSignal = true;
        return threatSignal;
    }

    private static void cleanChessBoard(int[][] chessField) {
        for (int i = 0; i < chessField.length; i++) {
            for (int j = 0; j < chessField.length; j++) {
                if (chessField[i][j] == 9) chessField[i][j] = 0;
            }
        }
    }

    private static void showChessField(int[][] chessField) {
        for (int y = 7; y >= 0; y--) {
            for (int x = 0; x < 8; x++) {
                System.out.print(chessField[x][y] + "  ");
            }
            System.out.println();
        }
    }

    private static void chessPrint_Of_Threat(boolean signal) {
        if (signal) {
            System.out.println("figure ab (2) threaten to figure cd (1)");
        }
    }

    private static boolean analyzeAndAction(int[][] chessField, int x, int y) {
        boolean threatSignal = false;

        switch (chessField[x][y]) {
            case 0:
                chessField[x][y] = 9;
                break;
            case 9:
                System.err.println("Please clean chess board or review chess logic");
                break;
            case 2:
                break;
            case 1:
                return threatSignal = true;
        }
        return false;
    }

    private static void testRook() {
        int[][] chessField = new int[8][8];

        int[] xABfigure = {2, 5, 5, 7};
        int[] yABfigure = {6, 7, 3, 6};

        System.out.println("Rook test.\nTest case: enemy figure CD (1)\n"
                + "comes to Rook AB (1) field.\nfigure ab (2) threaten to figure cd (1)\n*************************************");

        int xCDEnemyFigure = 5, yCDEnemyFigure = 6;
        chessField[xCDEnemyFigure][yCDEnemyFigure] = 1;

        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < 4; j++) {
                boolean threatToBCfigure = false;

                int x = xABfigure[j];
                int y = yABfigure[j];
                chessField[x][y] = 2;

                if (rookMovement(x, y, chessField.length, chessField))
                    System.out.printf("Rook (AB figure) with location x=%d y=%d "
                            + "thretened BC figure at location x=%d y=%d\n*************************************\n", x, y, xCDEnemyFigure, yCDEnemyFigure);
                chessField[x][y] = 0;
            }
        }
    }

    private static void testBishop() {
        int[][] chessField = new int[8][8];

        int[] xABfigure = {2, 6, 1, 7};
        int[] yABfigure = {6, 6, 1, 1};

        System.out.println("Bishop test.\nTest case: enemy figure CD (1)\n"
                + "comes to Bishop AB (1) field.\nfigure ab (2) threaten to figure cd (1)\n*************************************");

        int xCDEnemyFigure = 4, yCDEnemyFigure = 4;
        chessField[xCDEnemyFigure][yCDEnemyFigure] = 1;

        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < 4; j++) {
                boolean threatToBCfigure = false;

                int x = xABfigure[j];
                int y = yABfigure[j];
                chessField[x][y] = 2;

                if (bishopMovement(x, y, chessField.length, chessField))
                    System.out.printf("Bishop (AB figure) with location x=%d y=%d "
                            + "thretened BC figure at location x=%d y=%d\n*************************************\n", x, y, xCDEnemyFigure, yCDEnemyFigure);
                chessField[x][y] = 0;
            }
        }
    }

    private static void testKing() {
        int[][] chessField = new int[8][8];

        int[] xABfigure = {3, 5, 3, 5};
        int[] yABfigure = {5, 5, 3, 3};

        System.out.println("King test.\nTest case: enemy figure CD (1)\n"
                + "comes to King AB (1) field.\nfigure ab (2) threaten to figure cd (1)\n*************************************");

        int xCDEnemyFigure = 4, yCDEnemyFigure = 4;
        chessField[xCDEnemyFigure][yCDEnemyFigure] = 1;

        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < 4; j++) {
                boolean threatToBCfigure = false;

                int x = xABfigure[j];
                int y = yABfigure[j];
                chessField[x][y] = 2;

                if (kingMovement(x, y, chessField.length, chessField))
                    System.out.printf("King (AB figure) with location x=%d y=%d "
                            + "thretened BC figure at location x=%d y=%d\n*************************************\n", x, y, xCDEnemyFigure, yCDEnemyFigure);
                chessField[x][y] = 0;
            }
        }
    }

    private static void testQueen() {
        int[][] chessField = new int[8][8];

        int[] xABfigure = {1, 4, 7, 0, 7, 1, 4, 7};
        int[] yABfigure = {7, 7, 7, 4, 4, 1, 1, 1};

        System.out.println("Queen test.\nTest case: enemy figure CD (1)\n"
                + "comes to Queen AB (1) field.\nfigure ab (2) threaten to figure cd (1)\n*************************************");

        int xCDEnemyFigure = 4, yCDEnemyFigure = 4;
        chessField[xCDEnemyFigure][yCDEnemyFigure] = 1;

        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < 8; j++) {
                boolean threatToBCfigure = false;

                int x = xABfigure[j];
                int y = yABfigure[j];
                chessField[x][y] = 2;

                if (queenMovement(x, y, chessField.length, chessField))
                    System.out.printf("Queen (AB figure) with location x=%d y=%d "
                            + "thretened BC figure at location x=%d y=%d\n*************************************\n", x, y, xCDEnemyFigure, yCDEnemyFigure);
                chessField[x][y] = 0;
            }
        }
    }

    private static void testBlackPawn() {
        int[][] chessField = new int[8][8];

        int[] xABfigure = {3, 5, 4};
        int[] yABfigure = {5, 5, 5};

        System.out.println("blackPawn test.\nTest case: enemy figure CD (1)\n"
                + "comes to blackPawn AB (1) field.\nfigure ab (2) threaten to figure cd (1)\n*************************************");

        int xCDEnemyFigure = 4, yCDEnemyFigure = 4;
        chessField[xCDEnemyFigure][yCDEnemyFigure] = 1;

        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < 3; j++) {
                boolean threatToBCfigure = false;

                int x = xABfigure[j];
                int y = yABfigure[j];
                chessField[x][y] = 2;

                if (blackPawnMovement(x, y, chessField.length, chessField))
                    System.out.printf("blackPawn (AB figure) with location x=%d y=%d "
                            + "thretened BC figure at location x=%d y=%d\n*************************************\n", x, y, xCDEnemyFigure, yCDEnemyFigure);
                chessField[x][y] = 0;
            }
        }
    }

    private static void testWhitePawn() {
        int[][] chessField = new int[8][8];

        int[] xABfigure = {3, 5, 4};
        int[] yABfigure = {3, 3, 3};

        System.out.println("whitePawn test.\nTest case: enemy figure CD (1)\n"
                + "comes to whitePawn AB (1) field.\nfigure ab (2) threaten to figure cd (1)\n*************************************");

        int xCDEnemyFigure = 4, yCDEnemyFigure = 4;
        chessField[xCDEnemyFigure][yCDEnemyFigure] = 1;

        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < 3; j++) {
                boolean threatToBCfigure = false;

                int x = xABfigure[j];
                int y = yABfigure[j];
                chessField[x][y] = 2;

                if (whitePawnMovement(x, y, chessField.length, chessField))
                    System.out.printf("whitePawn (AB figure) with location x=%d y=%d "
                            + "thretened BC figure at location x=%d y=%d\n*************************************\n", x, y, xCDEnemyFigure, yCDEnemyFigure);
                chessField[x][y] = 0;
            }
        }
    }

    private static void testKnight() {
        int[][] chessField = new int[8][8];

        int[] xABfigure = {2, 3, 5, 6, 6, 5, 3, 2};
        int[] yABfigure = {5, 6, 6, 5, 3, 2, 2, 3};

        System.out.println("Knight test.\nTest case: enemy figure CD (1)\n"
                + "comes to Knight AB (1) field.\nfigure ab (2) threaten to figure cd (1)\n*************************************");

        int xCDEnemyFigure = 4, yCDEnemyFigure = 4;
        chessField[xCDEnemyFigure][yCDEnemyFigure] = 1;

        for (int j = 0; j < 8; j++) {
            boolean threatToBCfigure = false;

            int x = xABfigure[j];
            int y = yABfigure[j];
            chessField[x][y] = 2;

            if (knightMovement(x, y, chessField.length, chessField))
                System.out.printf("Knight (AB figure) with location x=%d y=%d "
                        + "thretened BC figure at location x=%d y=%d\n*************************************\n", x, y, xCDEnemyFigure, yCDEnemyFigure);
            chessField[x][y] = 0;
        }
    }
}