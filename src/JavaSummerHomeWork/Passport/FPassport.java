package JavaSummerHomeWork.Passport;

class FPassport extends PassportUA{

    //describes amount of visa in foreign passport
    public static final int AMNT_VISA_PAGES =2;

    //used while adding new visa to foreign passport
    private int visaCounter;

    //Passport serial for foreign passport
    String passportSerial;

    FPassport(String firstName, String familyName, String birthDate, String passportSerial){
        super(firstName,familyName,birthDate,null);
        this.passportSerial=passportSerial;
    }

    private Visa[] visas =new Visa[AMNT_VISA_PAGES];

    /**
     * Add new visa to foreign passport.
     * Shows warning if try to add visa more than pages in foreign passport
     * @param visaType
     * @param issuedDate
     * @param validTillDate
     */
    public void stickNewVisaToFPassport(String visaType, String issuedDate, String validTillDate){

        if(visaCounter<AMNT_VISA_PAGES) {
            visas[visaCounter++] = new Visa(visaType, issuedDate,validTillDate);
        }else System.err.printf("***********************************************************************%n" +
                        "Cannot add visa to %s %s %s%n" +
                        "All visa pages are occupied. Please change your passport first%n" +
                        "Cancelled request is:"+
                        "Visa type: %s%n" +
                        "Requested visa from date: %s%n" +
                        "Requested visa until date: %s%n",
                getFirstName(),getFamilyName(),getPassportSerial(),
                visaType,issuedDate,validTillDate);
    }

    public String getPassportSerial(){
        return passportSerial;
    }

    public Visa[] getVisas() {
        return visas.clone();
    }
}
