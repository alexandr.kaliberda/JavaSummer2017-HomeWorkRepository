package JavaSummerHomeWork.Passport;

//Passport_HW_1504722319
//Создайте класс Passport, который будет содержать паспортные данные гражданина Украины.
//С помощью наследования (расширения), создайте класс ForeignPassport (паспорт для выезда за границу)
//производный от Passport. Загранпаспорт содержит данные о визах и номер загранпаспорта.
//Визу рекомендую реализовать как отдельный класс и типом визы и датой открытия и закрытия
//(плюс дополнительные свойства на ваше усмотрение)

class Passport_Main {

    public static void main(String[] args) {

        System.out.println("TEST PART *** TEST PART *** TEST PART *** TEST PART *** TEST PART *** TEST PART *** \n");
        System.out.println("Test of stickNewVisaToFPassport() method.");

        {
            System.out.println("Test_1 Expected: Adding first and second visa to passport, without errors.\n" +
                    "Passport has space for 2 visa only. Visual comparation only.");

            //Initial information about passport holders
            String[] testNames = {"Andrew", "Artem", "Eva"};
            String[] testFamilyName = {"KUZMENKO", "TKACHUK", "POLISHUK"};
            String[] testBirthDate = {"10.1.1995", "11.2.1996", "12.3.1997"};
            String[] testPassportSerial = {"MU123", "MU124", "MU125"};

            //Initial information about first visa
            String[] testVisaType = {"Student", "Business", "Tourist"};
            String[] testFirstVisaIssuedDate = {"11.10.2017", "11.10.2017", "11.10.2017"};
            String[] testFirstVisaValidUntill = {"11.10.2018", "11.10.2021", "21.10.2017"};

            //Initial information about second visa
            String[] testSecondVisaType = {"Business", "Business", "Student"};
            String[] testSecondVisaIssuedDate = {"11.10.2019", "11.10.2019", "11.10.2019"};
            String[] testSecondVisaValidUntill = {"11.11.2019", "11.10.2020", "21.10.2019"};

            //Creating array of foreign passports
            FPassport[] fp = new FPassport[testNames.length];

            //Initializing foreign passport by name, family name, birth day.
            for (int i = 0; i < fp.length; i++) {
                fp[i] = new FPassport(testNames[i], testFamilyName[i], testBirthDate[i], testPassportSerial[i]);
            }

            //Initializing foreign passport by first viza
            for (int i = 0; i < fp.length; i++) {
                fp[i].stickNewVisaToFPassport(testVisaType[i], testFirstVisaIssuedDate[i], testFirstVisaValidUntill[i]);
            }

            //Initializing foreign passport by second viza
            for (int i = 0; i < fp.length; i++) {
                fp[i].stickNewVisaToFPassport(testSecondVisaType[i], testSecondVisaIssuedDate[i], testSecondVisaValidUntill[i]);
            }

            System.out.println("*********************************************************************************");
            System.out.println("Page Visa-holder_______    FPSerial  VisaIssued  ValidUntill VisaType");
            for (int i = 0; i < fp.length; i++) {
                if (fp[i] != null)
                    lookAtVisaPages(fp[i]);
            }

            System.out.println("Test passed.\n");
        }

        {
            System.out.println("*********************************************************************************");
            System.out.println("Test_2. Adding third visa inside of two pages passport\n" +
                    "Expected: Warning message and request to create new visa.\n" +
                    "Visual comparison only.");

            FPassport fp1 = new FPassport("Andrew","KUZMENKO","10.1.1995","MU123");
            fp1.stickNewVisaToFPassport("Student1","First date","First date");
            fp1.stickNewVisaToFPassport("Student2","Second date","Second date");
            fp1.stickNewVisaToFPassport("Student3","Third date","Third date");

            lookAtVisaPages(fp1);
            System.out.println("Test passed.\n");
        }


        {
            System.out.println("*********************************************************************************");
            System.out.println("Test_3. Showing first page of foreign passport\n" +
                    "Expected: It will show Name and Family Name, Passport serial number and BirtDate.\n" +
                    "Visual comparison only.");

            FPassport fp2 = new FPassport("Petya","Vovachkin","10.1.1995","MU123");
            showFPsprtFirstPage(fp2);
            System.out.println("Test passed.\n");
        }

    }

    public static void lookAtVisaPages(FPassport fp){
        for (int i = 0; i < fp.getVisas().length; i++) {
            if(fp.getVisas()[i]!=null) {
                System.out.printf("%-5d%-10s%-12s%-10s%-12s%s\t%-30s%n",
                        i+1,
                        fp.getFirstName(),fp.getFamilyName(),
                        fp.getPassportSerial(),
                        fp.getVisas()[i].getIssuedDate(),
                        fp.getVisas()[i].getValidTillDate(),
                        fp.getVisas()[i].getVisaType());
            }
            else System.out.printf("*********************************************************************************%n" +
                    "%s %s %s. Visa page %d is empty%n",fp.getFirstName(),fp.getFamilyName(),fp.getPassportSerial(),i+1);
        }
    }

    public static void showFPsprtFirstPage(FPassport fp){
        System.out.printf("***********************************************************************%n" +
                        "Foreign Passport first page.%n" +
                        "Foreign Passport Serial Number: %s%n" +
                        "First Name: %s, Family Name: %s%n" +
                        "BirthDate %s\n"
                ,fp.getPassportSerial(),fp.getFirstName(),fp.getFamilyName(),fp.getBirthDate());
    }
}
