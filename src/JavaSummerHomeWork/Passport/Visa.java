package JavaSummerHomeWork.Passport;

class Visa{
    private String visaType, issuedDate, validTillDate;

    public Visa(String visaType, String issuedDate, String validTillDate) {
        this.visaType = visaType;
        this.issuedDate=issuedDate;
        this.validTillDate=validTillDate;
    }

    public String getVisaType() {
        return visaType;
    }
    public String getIssuedDate() {
        return issuedDate;
    }
    public String getValidTillDate() {
        return validTillDate;
    }
}
