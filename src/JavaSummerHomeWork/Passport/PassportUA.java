package JavaSummerHomeWork.Passport;

class PassportUA{
    private String firstName, familyName, birthDate, passportSerial;

    PassportUA(String firstName, String familyName, String birthDate, String passportSerial){
        this.firstName=firstName;
        this.familyName=familyName;
        this.birthDate=birthDate;
        this.passportSerial=passportSerial;
    }

    public String getFirstName() {
        return firstName;
    }
    public String getFamilyName() {
        return familyName;
    }
    public String getBirthDate() {
        return birthDate;
    }
}
