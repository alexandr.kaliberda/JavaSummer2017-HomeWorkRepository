/*
DML_HW_1511378864
Заполнить таблицы, которые созданы в предыдущем
домашнем задании, данными. Чем больше и разнообразнее
будут данные в таблицах, тем лучше будут результаты
при отработке селект запросов.
*/

    USE school;

  INSERT INTO passport
  (passport_id,first_name,last_name) VALUES
  (NULL,'MILLER','Dmitry'),
  (NULL,'SHEVCHUK','Solomiya'),
  (NULL,'IVANOVA','Maria'),
  (NULL,'KRAVCHUK','Veronika'),
  (NULL,'LISENKO','Andrew'),
  (NULL,'SAVCHENKO','Anastasia'),
  (NULL,'KOVALENKO','Michael'),
  (NULL,'FROST','Daniel'),
  (NULL,'PONOMARENKO','Ivan'),
  (NULL,'Rudenko','Dmitry'),
  (NULL,'KHOMENKO','Daniel'),
  (NULL,'Kharchenko','Solomiya'),
  (NULL,'TKACHENKO','Vladislav'),
  (NULL,'MELINICHUK','Eva'),
  (NULL,'KOVAL','Ivan'),
  (NULL,'COLOMY','Solomiya'),
  (NULL,'LITENKO','Artem'),
  (NULL,'KLIMENKO','Victoria'),
  (NULL,'SHEVCHENKO','Veronika'),
  (NULL,'Kravchenko','Daria'),
  (NULL,'MARCHENKO','Zlata'),
  (NULL,'PETRENKO','Dmitry'),
  (NULL,'KOVALCHUK','Artem'),
  (NULL,'GAVRILYUK','Daria'),
  (NULL,'LEVCHENKO','Zlata'),
  (NULL,'PAVLYUK','Ivan'),
  (NULL,'ROMANYUK','Daria'),
  (NULL,'BONDARENKO','Denis'),
  (NULL,'Pavlenko','Michael'),
  (NULL,'SAVCHUK','Denis'),
  (NULL,'OLIVNIK','Maria'),
  (NULL,'KARPENKO','Andrew'),
  (NULL,'BOYKO','Victoria'),
  (NULL,'BONDAR','Alexander'),
  (NULL,'VASILENKO','Alexander'),
  (NULL,'MARTINYUK','Maria'),
  (NULL,'WEAVER','Eva'),
  (NULL,'MAZUR','Sofia'),
  (NULL,'KUZMENKO','Vladislav'),
  (NULL,'TKACHUK','Sofia'),
  (NULL,'KUSHNIR','Dmitry'),
  (NULL,'POLISHCHUK','Eva'),
  (NULL,'SIDORENKO','Anastasia');

  INSERT INTO students
  (student_id, passport_id, entry_date)VALUES
    (NULL,49,'2016-12-24'),
    (NULL,50,'2016-12-24'),
    (NULL,51,'2016-12-24'),
    (NULL,52,'2016-12-24'),
    (NULL,53,'2016-12-24'),
    (NULL,54,'2016-12-24'),
    (NULL,55,'2016-12-24'),
    (NULL,56,'2016-12-24'),
    (NULL,57,'2016-12-24'),
    (NULL,58,'2016-12-24'),
    (NULL,59,'2017-10-03'),
    (NULL,60,'2017-10-03'),
    (NULL,61,'2017-10-03'),
    (NULL,62,'2017-10-03'),
    (NULL,63,'2017-10-03'),
    (NULL,64,'2017-10-03'),
    (NULL,65,'2017-10-03'),
    (NULL,66,'2017-10-03'),
    (NULL,67,'2017-10-03'),
    (NULL,68,'2017-10-03'),
    (NULL,69,'2017-10-03'),
    (NULL,70,'2017-10-03'),
    (NULL,71,'2017-10-03'),
    (NULL,72,'2017-10-03'),
    (NULL,73,'2017-10-03'),
    (NULL,74,'2017-10-03'),
    (NULL,75,'2017-10-03'),
    (NULL,76,'2017-10-03'),
    (NULL,77,'2017-10-03'),
    (NULL,78,'2017-10-03'),
    (NULL,79,'2017-10-03'),
    (NULL,80,'2017-10-03'),
    (NULL,81,'2017-10-03'),
    (NULL,82,'2017-10-03'),
    (NULL,83,'2017-10-03');

    INSERT INTO teachers
    (teacher_id, passport_id)VALUES
    (NULL,84),
    (NULL,85),
    (NULL,86),
    (NULL,87),
    (NULL,88),
    (NULL,89),
    (NULL,90),
    (NULL,91);


    -- student_id, group_id
    INSERT INTO student_groups
    (student_id, group_id) VALUES
    (111,2),
    (112,2),
    (113,2),
    (114,2),
    (115,2),
    (116,2),
    (117,2),
    (118,2),
    (119,2),
    (120,2),
    (121,1),
    (122,1),
    (123,1),
    (124,1),
    (125,1),
    (126,1),
    (127,1),
    (128,1),
    (129,1),
    (130,1),
    (131,1),
    (132,1),
    (133,1),
    (134,1),
    (135,1),
    (136,1),
    (137,1),
    (138,1),
    (139,1),
    (140,1),
    (141,1),
    (142,1),
    (143,1),
    (144,1),
    (145,1);


    INSERT INTO subjects
    (subject_id, subject_name) VALUES
    (NULL, 'Classes');

    INSERT INTO marks
    (marks_id, student_id, group_id, subject_id, mark, date, teacher_id) VALUES
    (NULL,111,2,2,9,'2017-12-01',2),
    (NULL,112,2,2,6,'2017-12-01',2),
    (NULL,113,2,2,5,'2017-12-01',2),
    (NULL,114,2,2,8,'2017-12-01',2),
    (NULL,115,2,2,11,'2017-12-01',2),
    (NULL,116,2,2,5,'2017-12-01',2),
    (NULL,117,2,2,5,'2017-12-01',2),
    (NULL,118,2,2,10,'2017-12-01',2),
    (NULL,119,2,2,8,'2017-12-01',2),
    (NULL,120,2,2,10,'2017-12-01',2),
    (NULL,121,1,1,7,'2017-11-20',1),
    (NULL,122,1,1,11,'2017-11-20',1),
    (NULL,123,1,1,7,'2017-11-20',1),
    (NULL,124,1,1,5,'2017-11-20',1),
    (NULL,125,1,1,5,'2017-11-20',1),
    (NULL,126,1,1,11,'2017-11-20',1),
    (NULL,127,1,1,5,'2017-11-20',1),
    (NULL,128,1,1,8,'2017-11-20',1),
    (NULL,129,1,1,4,'2017-11-20',1),
    (NULL,130,1,1,12,'2017-11-20',1),
    (NULL,131,1,1,12,'2017-11-20',1),
    (NULL,132,1,1,7,'2017-11-20',1),
    (NULL,133,1,1,5,'2017-11-20',1),
    (NULL,134,1,1,12,'2017-11-20',1),
    (NULL,135,1,3,8,'2017-11-21',1),
    (NULL,136,1,3,12,'2017-11-21',1),
    (NULL,137,1,3,8,'2017-11-21',1),
    (NULL,138,1,3,6,'2017-11-21',1),
    (NULL,139,1,3,7,'2017-11-21',1),
    (NULL,140,1,3,5,'2017-11-21',1),
    (NULL,141,1,3,5,'2017-11-21',1),
    (NULL,142,1,3,10,'2017-11-21',1),
    (NULL,143,1,3,5,'2017-11-21',1),
    (NULL,144,1,3,8,'2017-11-21',1),
    (NULL,145,1,3,9,'2017-11-21',1);


