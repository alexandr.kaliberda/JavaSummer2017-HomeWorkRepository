/*https://youtu.be/jybGVzjf8oc
Создать две таблицы - автомобили и автомобильные номера.
 Сделать в каждой таблице первичный ключ в виде id, и
 связять их внешним ключем.
В качестве результата прислать скрипт.*/

DROP DATABASE IF EXISTS transport;
CREATE DATABASE transport;

USE transport;

CREATE TABLE car
(
  car_id SERIAL PRIMARY KEY,
  made_year INT NOT NULL DEFAULT 0,
  color VARCHAR(12) NOT NULL DEFAULT 'Unknown',
  mark VARCHAR(12) NOT NULL DEFAULT 'Unknown'
)
  ENGINE InnoDB CHARACTER SET utf8;

CREATE TABLE car_number
(
  number_id SERIAL PRIMARY KEY,
  country VARCHAR(12) NOT NULL DEFAULT 'Unknown',
  district VARCHAR(2) NOT NULL DEFAULT '00',#distruct (Kyiv, Dnipro) 2 letters
  serial INT(4) NOT NULL DEFAULT 0000,#number
  serial_num VARCHAR(2) NOT NULL DEFAULT '00',#serial number 2 letters
  car_id BIGINT UNSIGNED,
  FOREIGN KEY (car_id)REFERENCES car(car_id)
    ON DELETE RESTRICT ON UPDATE CASCADE
)
  ENGINE InnoDB CHARACTER SET utf8;

INSERT INTO car VALUES
  (NULL ,2010,'yellow','toyota'),
  (NULL ,2014,'black','honda'),
  (NULL ,1979,'red','moskvich'),
  (NULL ,1955,'white','zaporozhec');

INSERT INTO car_number VALUES
  (NULL ,'Ukraine', 'AA',1245,'BC',(SELECT car_id FROM car WHERE made_year = 2010)),
  (NULL ,'Ukraine', 'AE',8911,'BC',(SELECT car_id FROM car WHERE made_year = 1955)),
  (NULL ,'Ukraine', 'AK',2137,'DE',(SELECT car_id FROM car WHERE made_year = 1979)),
  (NULL ,'Ukraine', 'AH',1234,'BC',(SELECT car_id FROM car WHERE made_year = 2014));
