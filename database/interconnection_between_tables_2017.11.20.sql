/*https://youtu.be/aPT8H9rTleI

Написать SQL скрипт для создания таблиц как показано на схеме в архиве.*/

DROP DATABASE IF EXISTS school;
CREATE DATABASE school;

USE school;

CREATE TABLE passport
(
  passport_id SERIAL PRIMARY KEY,
  first_name  VARCHAR(12) NOT NULL DEFAULT 'Unknown',
  last_name   VARCHAR(12) NOT NULL DEFAULT 'Unknown'
)
  ENGINE InnoDB
  CHARACTER SET utf8;


CREATE TABLE students
(
  student_id  SERIAL PRIMARY KEY,
  passport_id BIGINT UNSIGNED,
  entry_date  DATE NOT NULL DEFAULT '1970-01-01',

  FOREIGN KEY (passport_id) REFERENCES passport (passport_id)
    ON DELETE RESTRICT ON UPDATE CASCADE
)
  ENGINE InnoDB
  CHARACTER SET utf8;


CREATE TABLE teachers
(
  teacher_id  SERIAL PRIMARY KEY,
  passport_id BIGINT UNSIGNED,

  FOREIGN KEY (passport_id) REFERENCES passport (passport_id)
    ON DELETE RESTRICT ON UPDATE CASCADE
)
  ENGINE InnoDB
  CHARACTER SET utf8;



CREATE TABLE forms
(
  form_id   SERIAL PRIMARY KEY,
  form_name VARCHAR(12) NOT NULL DEFAULT 'Unknown'
)
  ENGINE InnoDB
  CHARACTER SET utf8;



CREATE TABLE groups
(
  group_id   SERIAL PRIMARY KEY,
  group_name VARCHAR(12) NOT NULL DEFAULT 'Unknown',
  form_id    BIGINT UNSIGNED,

  FOREIGN KEY (form_id) REFERENCES forms (form_id)
    ON DELETE RESTRICT ON UPDATE CASCADE
)
  ENGINE InnoDB
  CHARACTER SET utf8;


CREATE TABLE student_groups
(
  student_id BIGINT UNSIGNED,
  group_id   BIGINT UNSIGNED,
  FOREIGN KEY (student_id) REFERENCES students (student_id)
    ON DELETE RESTRICT ON UPDATE CASCADE,

  FOREIGN KEY (group_id) REFERENCES groups (group_id)
    ON DELETE RESTRICT ON UPDATE CASCADE
)
  ENGINE InnoDB
  CHARACTER SET utf8;



CREATE TABLE subjects
(
  subject_id   SERIAL PRIMARY KEY,
  subject_name VARCHAR(256)
)
  ENGINE InnoDB
  CHARACTER SET utf8;



CREATE TABLE subject_teacher
(
  subject_id BIGINT UNSIGNED,
  teacher_id BIGINT UNSIGNED,
  FOREIGN KEY (subject_id) REFERENCES subjects(subject_id)
    ON DELETE RESTRICT ON UPDATE CASCADE,

  FOREIGN KEY (teacher_id) REFERENCES teachers(teacher_id)
    ON DELETE RESTRICT ON UPDATE CASCADE
)
  ENGINE  InnoDB
  CHARACTER SET utf8;



CREATE TABLE marks
(
  marks_id SERIAL PRIMARY KEY,
  student_id BIGINT UNSIGNED,
  group_id BIGINT UNSIGNED,
  subject_id BIGINT UNSIGNED,
  mark TINYINT,
  date DATE,
  teacher_id BIGINT UNSIGNED,

  FOREIGN KEY (student_id)REFERENCES students(student_id)
    ON DELETE RESTRICT ON UPDATE CASCADE,

  FOREIGN KEY (group_id)REFERENCES groups(group_id)
    ON DELETE RESTRICT ON UPDATE CASCADE,

  FOREIGN KEY (subject_id)REFERENCES subjects(subject_id)
    ON DELETE RESTRICT ON UPDATE CASCADE,

  FOREIGN KEY (teacher_id)REFERENCES teachers(teacher_id)
    ON DELETE RESTRICT ON UPDATE CASCADE
)
  ENGINE  InnoDB
  CHARACTER SET utf8;